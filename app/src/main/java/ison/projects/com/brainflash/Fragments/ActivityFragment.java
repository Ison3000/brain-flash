package ison.projects.com.brainflash.Fragments;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pixplicity.easyprefs.library.Prefs;

import ison.projects.com.brainflash.Activities.LevelSelectActivity;
import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActivityFragment extends Fragment implements View.OnClickListener{


    private View mView;
    Button btn_num_tricks, btn_operation_math, btn_pattern_recognition, btn_build_o_words,
            btn_word_quiz, btn_syntax, btn_riddles, btn_sudoku_logic, btn_who_i_am;

    Intent myIntent;

    public ActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_activity, container, false);
        initUI();


        return mView;


    }

    public void initUI(){
        btn_num_tricks = (Button) mView.findViewById(R.id.btn_num_tricks);
        btn_num_tricks.setOnClickListener(this);
        btn_operation_math = (Button) mView.findViewById(R.id.btn_operation_math);
        btn_operation_math.setOnClickListener(this);
        btn_pattern_recognition = (Button) mView.findViewById(R.id.btn_pattern_recognition);
        btn_pattern_recognition.setOnClickListener(this);
        btn_build_o_words = (Button) mView.findViewById(R.id.btn_build_o_words);
        btn_build_o_words.setOnClickListener(this);
        btn_word_quiz = (Button) mView.findViewById(R.id.btn_word_quiz);
        btn_word_quiz.setOnClickListener(this);
        btn_syntax = (Button) mView.findViewById(R.id.btn_syntax);
        btn_syntax.setOnClickListener(this);
        btn_riddles = (Button) mView.findViewById(R.id.btn_riddles);
        btn_riddles.setOnClickListener(this);
        btn_sudoku_logic = (Button) mView.findViewById(R.id.btn_sudoku_logic);
        btn_sudoku_logic.setOnClickListener(this);
        btn_who_i_am = (Button) mView.findViewById(R.id.btn_who_i_am);
        btn_who_i_am.setOnClickListener(this);

    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(getActivity(), R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    @Override
    public void onClick(View view) {
     switch (view.getId()){
         case R.id.btn_num_tricks:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_MATH_NUMBER_TRICKS, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_operation_math:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_MATH_OPERATIONS, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_pattern_recognition:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_MATH_PATTERN_RECOGNITON, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_build_o_words:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_MEMORY_BUILD_O_WORDS, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_word_quiz:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_MEMORY_WORD_QUIZ, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_syntax:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_MEMORY_SYNTAX, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_riddles:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_LOGIC_RIDDLES, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_sudoku_logic:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_LOGIC_SOLUTIONS, AppConstants.ACTIVITY_GAME);
             break;

         case R.id.btn_who_i_am:
             btnClickSound();
             sendIntent(AppConstants.ACTIVITY_LOGIC_WHO_I_AM, AppConstants.ACTIVITY_GAME);
             break;

         default:
             break;

      }

    }


    public void sendIntent(String activity, String training){
        myIntent = new Intent(getActivity(), LevelSelectActivity.class);
        myIntent.putExtra(AppConstants.ACTIVITY, activity ); //Optional parameters
        myIntent.putExtra(AppConstants.GAME_TYPE, training ); //Optional parameters
        getActivity().startActivity(myIntent);
    }

}
