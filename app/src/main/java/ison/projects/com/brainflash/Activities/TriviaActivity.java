package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.pixplicity.easyprefs.library.Prefs;

import org.w3c.dom.Text;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class TriviaActivity extends AppCompatActivity {

    TextView trivia, score;
    int question_count, selectedDay;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trivia);
        Log.e("TAG", "onCreate: " + Prefs.getInt(AppConstants.SCORE_1, question_count ) );
        initUI();

    }

    public void initUI(){
        trivia = (TextView) findViewById(R.id.trivia);
        score = (TextView) findViewById(R.id.score);
        getIntentData();
    }


    public void getIntentData(){
        intent = getIntent();
        selectedDay = intent.getIntExtra(AppConstants.SELECTED_DAY, 0); //if it's a string you stored.
        Log.e("TRIVIA", "getIntentData: " + selectedDay );
        checkScores(selectedDay);
    }


    public void checkScores(int date){
        Log.e("TRIVIA", "checkScores: " + date);
        switch (date){

            case 1:
                if(Prefs.getInt(AppConstants.SCORE_1, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_1);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_1, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_1, 0 )));
                }
                break;
            case 2:
                if(Prefs.getInt(AppConstants.SCORE_2, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_2);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_2, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_2, 0 )));
                }
                break;
            case 3:
                if(Prefs.getInt(AppConstants.SCORE_3, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_3);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_3, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_3, 0 )));
                }
                break;
            case 4:
                if(Prefs.getInt(AppConstants.SCORE_4, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_4);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_4, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_4, 0 )));
                }
                break;
            case 5:
                if(Prefs.getInt(AppConstants.SCORE_5, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_5);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_5, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_5, 0 )));
                }
                break;
            case 6:
                if(Prefs.getInt(AppConstants.SCORE_6, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_6);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_6, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_6, 0 )));
                }
                break;
            case 7:
                if(Prefs.getInt(AppConstants.SCORE_7, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_7);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_7, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_7, 0 )));
                }
                break;
            case 8:
                if(Prefs.getInt(AppConstants.SCORE_8, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_8);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_8, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_8, 0 )));
                }
                Prefs.getInt(AppConstants.SCORE_8, question_count);
                break;
            case 9:
                if(Prefs.getInt(AppConstants.SCORE_9, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_9);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_9, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_9, 0 )));
                }
                break;
            case 10:
                if(Prefs.getInt(AppConstants.SCORE_10, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_10);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_10, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_10, 0 )));
                }
                break;
            case 11:
                if(Prefs.getInt(AppConstants.SCORE_11, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_11);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_11, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_11, 0 )));
                }
                break;
            case 12:
                if(Prefs.getInt(AppConstants.SCORE_12, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_12);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_12, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_12, 0 )));
                }
                break;
            case 13:
                if(Prefs.getInt(AppConstants.SCORE_13, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_13);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_13, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_13, 0 )));
                }
                break;
            case 14:
                if(Prefs.getInt(AppConstants.SCORE_14, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_14);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_14, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_14, 0 )));
                }
                break;
            case 15:
                if(Prefs.getInt(AppConstants.SCORE_15, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_15);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_15, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_15, 0 )));
                }
                break;
            case 16:
                if(Prefs.getInt(AppConstants.SCORE_16, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_16);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_16, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_16, 0 )));
                }
                break;
            case 17:
                if(Prefs.getInt(AppConstants.SCORE_17, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_17);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_17, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_17, 0 )));
                }
                break;
            case 18:
                if(Prefs.getInt(AppConstants.SCORE_18, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_18);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_18, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_18, 0 )));
                }
                break;
            case 19:
                if(Prefs.getInt(AppConstants.SCORE_19, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_19);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_19, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_19, 0 )));
                }
                break;
            case 20:
                if(Prefs.getInt(AppConstants.SCORE_20, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_20);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_20, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_20, 0 )));
                }
                break;
            case 21:
                if(Prefs.getInt(AppConstants.SCORE_21, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_21);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_21, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_21, 0 )));
                }
                break;
            case 22:
                if(Prefs.getInt(AppConstants.SCORE_22, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_22);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_22, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_22, 0 )));
                }
                break;
            case 23:
                if(Prefs.getInt(AppConstants.SCORE_23, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_23);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_23, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_23, 0 )));
                }
                break;
            case 24:
                if(Prefs.getInt(AppConstants.SCORE_24, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_24);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_24, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_24, 0 )));
                }
                break;
            case 25:
                if(Prefs.getInt(AppConstants.SCORE_25, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_25);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_25, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_25, 0 )));
                }
                break;
            case 26:
                if(Prefs.getInt(AppConstants.SCORE_26, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_26);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_26, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_26, 0 )));
                }
                break;
            case 27:
                if(Prefs.getInt(AppConstants.SCORE_27, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_27);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_27, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_27, 0 )));
                }
                break;
            case 28:
                if(Prefs.getInt(AppConstants.SCORE_28, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_28);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_28, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_28, 0 )));
                }
                break;
            case 29:
                if(Prefs.getInt(AppConstants.SCORE_29, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_29);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_29, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_29, 0 )));
                }
                break;
            case 30:
                if(Prefs.getInt(AppConstants.SCORE_30, 0) >= AppConstants.QUESTION_TARGET){
                    trivia.setText(AppConstants.TRIVIA_30);
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_30, 0 )));
                }
                else{
                    score.setText(String.valueOf(Prefs.getInt(AppConstants.SCORE_30, 0 )));
                }
                break;

            default:
                break;
        }
    }

}
