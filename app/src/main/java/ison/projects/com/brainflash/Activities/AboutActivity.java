package ison.projects.com.brainflash.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ison.projects.com.brainflash.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
