package ison.projects.com.brainflash.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ison.projects.com.brainflash.R;

import java.util.ArrayList;
import java.util.List;

public class TabFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private TabLayout mTabLayout;

    @SuppressWarnings("deprecation")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View tabView = inflater.inflate(R.layout.tab_layout, container, false);

        mTabLayout = (TabLayout) tabView.findViewById(R.id.tabs);
        ViewPager mViewPager = (ViewPager) tabView.findViewById(R.id.viewpager);
        mTabLayout.addTab(mTabLayout.newTab().setText("Training").setIcon(R.drawable.training_icon));
        mTabLayout.addTab(mTabLayout.newTab().setText("Activity").setIcon(R.drawable.activity_icon));
        mTabLayout.addTab(mTabLayout.newTab().setText("TBOT").setIcon(R.drawable.tbot_icon));
        mTabLayout.addTab(mTabLayout.newTab().setText("Options").setIcon(R.drawable.option_icon));

        PagerAdapter mPagerAdapter = new PagerAdapter(getFragmentManager());
        mPagerAdapter.addFragment(new TrainingFragment());
        mPagerAdapter.addFragment(new ActivityFragment());
        mPagerAdapter.addFragment(new TbotFragment());
        mPagerAdapter.addFragment(new OptionFragment());

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mViewPager.addOnPageChangeListener(this);
        mTabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        return tabView;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        TabLayout.Tab tab0 = mTabLayout.getTabAt(0);
        TabLayout.Tab tab1 = mTabLayout.getTabAt(1);
        TabLayout.Tab tab2 = mTabLayout.getTabAt(2);
        TabLayout.Tab tab3 = mTabLayout.getTabAt(3);
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    class PagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragments = new ArrayList<>();

        private PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private void addFragment(Fragment fragment) {
            mFragments.add(fragment);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}