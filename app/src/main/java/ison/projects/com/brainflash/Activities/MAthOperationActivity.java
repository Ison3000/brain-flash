package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class MAthOperationActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MAthOperationActivity";
    Intent intent;
    String value, game_type;
    int score, numQuestions;
    TextView textView_problem, timer;
    Button btn_plus, btn_minus, btn_multiply, btn_divide;
    String answer;
    MediaPlayer mp, mpWrong, mpCorrect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_operation);

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        getIntentData();
    }


    public void initUI(){
        timer = (TextView) findViewById(R.id.timer);
        btn_plus = (Button) findViewById(R.id.btn_plus);
        btn_plus.setOnClickListener(this);
        btn_minus = (Button) findViewById(R.id.btn_minus);
        btn_minus.setOnClickListener(this);
        btn_multiply = (Button) findViewById(R.id.btn_multiply);
        btn_multiply.setOnClickListener(this);
        btn_divide = (Button) findViewById(R.id.btn_divide);
        btn_divide.setOnClickListener(this);
        textView_problem = (TextView) findViewById(R.id.textView_problem);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score);
        refactorUI();

        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }

    public void refactorUI(){
        switch (value){
            case "1":
                answer = AppConstants.MATH_OPERATION_ANSWER_1;
                textView_problem.setText(AppConstants.MATH_OPERATION_1);
                break;

            case "2":
                answer = AppConstants.MATH_OPERATION_ANSWER_2;
                textView_problem.setText(AppConstants.MATH_OPERATION_2);
                break;

            case "3":
                answer = AppConstants.MATH_OPERATION_ANSWER_3;
                textView_problem.setText(AppConstants.MATH_OPERATION_3);
                break;

            case "4":
                answer = AppConstants.MATH_OPERATION_ANSWER_4;
                textView_problem.setText(AppConstants.MATH_OPERATION_4);
                break;

            case "5":
                answer = AppConstants.MATH_OPERATION_ANSWER_5;
                textView_problem.setText(AppConstants.MATH_OPERATION_5);
                break;

            case "6":
                answer = AppConstants.MATH_OPERATION_ANSWER_6;
                textView_problem.setText(AppConstants.MATH_OPERATION_6);
                break;

            case "7":
                answer = AppConstants.MATH_OPERATION_ANSWER_7;
                textView_problem.setText(AppConstants.MATH_OPERATION_7);
                break;

            case "8":
                answer = AppConstants.MATH_OPERATION_ANSWER_8;
                textView_problem.setText(AppConstants.MATH_OPERATION_8);
                break;

            case "9":
                answer = AppConstants.MATH_OPERATION_ANSWER_9;
                textView_problem.setText(AppConstants.MATH_OPERATION_9);
                break;

            case "10":
                answer = AppConstants.MATH_OPERATION_ANSWER_10;
                textView_problem.setText(AppConstants.MATH_OPERATION_10);
                break;

            case "11":
                answer = AppConstants.MATH_OPERATION_ANSWER_11;
                textView_problem.setText(AppConstants.MATH_OPERATION_11);
                break;

            case "12":
                answer = AppConstants.MATH_OPERATION_ANSWER_12;
                textView_problem.setText(AppConstants.MATH_OPERATION_12);
                break;

            case "13":
                answer = AppConstants.MATH_OPERATION_ANSWER_13;
                textView_problem.setText(AppConstants.MATH_OPERATION_13);
                break;

            case "14":
                answer = AppConstants.MATH_OPERATION_ANSWER_14;
                textView_problem.setText(AppConstants.MATH_OPERATION_14);
                break;

            case "15":
                answer = AppConstants.MATH_OPERATION_ANSWER_15;
                textView_problem.setText(AppConstants.MATH_OPERATION_15);
                break;

            case "16":
                answer = AppConstants.MATH_OPERATION_ANSWER_16;
                textView_problem.setText(AppConstants.MATH_OPERATION_16);
                break;

            case "17":
                answer = AppConstants.MATH_OPERATION_ANSWER_17;
                textView_problem.setText(AppConstants.MATH_OPERATION_17);
                break;

            case "18":
                answer = AppConstants.MATH_OPERATION_ANSWER_18;
                textView_problem.setText(AppConstants.MATH_OPERATION_18);
                break;

            case "19":
                answer = AppConstants.MATH_OPERATION_ANSWER_19;
                textView_problem.setText(AppConstants.MATH_OPERATION_19);
                break;

            case "20":
                answer = AppConstants.MATH_OPERATION_ANSWER_20;
                textView_problem.setText(AppConstants.MATH_OPERATION_20);
                break;

            case "21":
                answer = AppConstants.MATH_OPERATION_ANSWER_21;
                textView_problem.setText(AppConstants.MATH_OPERATION_21);
                break;

            case "22":
                answer = AppConstants.MATH_OPERATION_ANSWER_22;
                textView_problem.setText(AppConstants.MATH_OPERATION_22);
                break;

            case "23":
                answer = AppConstants.MATH_OPERATION_ANSWER_23;
                textView_problem.setText(AppConstants.MATH_OPERATION_23);
                break;

            case "24":
                answer = AppConstants.MATH_OPERATION_ANSWER_24;
                textView_problem.setText(AppConstants.MATH_OPERATION_24);
                break;

            case "25":
                answer = AppConstants.MATH_OPERATION_ANSWER_25;
                textView_problem.setText(AppConstants.MATH_OPERATION_25);
                break;

            case "26":
                answer = AppConstants.MATH_OPERATION_ANSWER_26;
                textView_problem.setText(AppConstants.MATH_OPERATION_26);
                break;

            case "27":
                answer = AppConstants.MATH_OPERATION_ANSWER_27;
                textView_problem.setText(AppConstants.MATH_OPERATION_27);
                break;

            case "28":
                answer = AppConstants.MATH_OPERATION_ANSWER_28;
                textView_problem.setText(AppConstants.MATH_OPERATION_28);
                break;

            case "29":
                answer = AppConstants.MATH_OPERATION_ANSWER_29;
                textView_problem.setText(AppConstants.MATH_OPERATION_29);
                break;

            case "30":
                answer = AppConstants.MATH_OPERATION_ANSWER_30;
                textView_problem.setText(AppConstants.MATH_OPERATION_30);
                break;

            default:
                break;
        }
    }

    public void verifyAnswer(Button btn_name){
        switch (value){
            case "1":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_1_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "2":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_2_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "3":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_3_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "4":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_4_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "5":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_5_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "6":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_6_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "7":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_7_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "8":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_8_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "9":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_9_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "10":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_10_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "11":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_11_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "12":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_12_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "13":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_13_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "14":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_14_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "15":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_15_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "16":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_16_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "17":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_17_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "18":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_18_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "19":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_19_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "20":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_20_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "21":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_21_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "22":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_22_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "23":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_23_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "24":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_24_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "25":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_25_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "26":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_26_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "27":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_27_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "28":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_28_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "29":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_29_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "30":
                if(btn_name.getText().toString().equals(answer)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.OPERATION_30_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }break;
            default:
                break;
        }
    }

    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.
        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.
    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_plus:
                btnClickSound();
                verifyAnswer(btn_plus);
                break;

            case R.id.btn_minus:
                btnClickSound();
                verifyAnswer(btn_minus );
                break;

            case R.id.btn_multiply:
                btnClickSound();
                verifyAnswer(btn_multiply);
                break;

            case R.id.btn_divide:
                btnClickSound();
                verifyAnswer(btn_divide);
                break;

            default:
                break;
        }

    }

    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }
    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");
                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }
}
