package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.w3c.dom.Text;

import es.dmoral.toasty.Toasty;
import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class LogicRiddlesActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "LogicRiddlesActivity";
    Intent intent;
    String value, game_type;
    int score, numQuestions;
    TextView question, timer;
    EditText editText_Answer;
    String answer;
    Button btn_submit;
    public MediaPlayer mp, mpCorrect, mpWrong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logic_riddles);

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        getIntentData();

        initUI();
        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }

    public void initUI() {
        question = (TextView) findViewById(R.id.question);
        timer = (TextView) findViewById(R.id.timer);
        editText_Answer = (EditText) findViewById(R.id.editText_answer);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score + " " + numQuestions);
        refactorUI();
    }

    public void refactorUI() {
        switch (value) {
            case "1":
                answer = AppConstants.RIDDLES_ANSwER_1;
                question.setText(AppConstants.RIDDLES_QUESTION_1);
                break;

            case "2":
                answer = AppConstants.RIDDLES_ANSwER_2;
                question.setText(AppConstants.RIDDLES_QUESTION_2);
                break;

            case "3":
                answer = AppConstants.RIDDLES_ANSwER_3;
                question.setText(AppConstants.RIDDLES_QUESTION_3);
                break;

            case "4":
                answer = AppConstants.RIDDLES_ANSwER_4;
                question.setText(AppConstants.RIDDLES_QUESTION_4);
                break;

            case "5":
                answer = AppConstants.RIDDLES_ANSwER_5;
                question.setText(AppConstants.RIDDLES_QUESTION_5);
                break;

            case "6":
                answer = AppConstants.RIDDLES_ANSwER_6;
                question.setText(AppConstants.RIDDLES_QUESTION_6);
                break;

            case "7":
                answer = AppConstants.RIDDLES_ANSwER_7;
                question.setText(AppConstants.RIDDLES_QUESTION_7);
                break;

            case "8":
                answer = AppConstants.RIDDLES_ANSwER_8;
                question.setText(AppConstants.RIDDLES_QUESTION_8);
                break;

            case "9":
                answer = AppConstants.RIDDLES_ANSwER_9;
                question.setText(AppConstants.RIDDLES_QUESTION_9);
                break;

            case "10":
                answer = AppConstants.RIDDLES_ANSwER_10;
                question.setText(AppConstants.RIDDLES_QUESTION_10);
                break;

            case "11":
                answer = AppConstants.RIDDLES_ANSwER_11;
                question.setText(AppConstants.RIDDLES_QUESTION_11);
                break;

            case "12":
                answer = AppConstants.RIDDLES_ANSwER_12;
                question.setText(AppConstants.RIDDLES_QUESTION_12);
                break;

            case "13":
                answer = AppConstants.RIDDLES_ANSwER_13;
                question.setText(AppConstants.RIDDLES_QUESTION_13);
                break;

            case "14":
                answer = AppConstants.RIDDLES_ANSwER_14;
                question.setText(AppConstants.RIDDLES_QUESTION_14);
                break;

            case "15":
                answer = AppConstants.RIDDLES_ANSwER_15;
                question.setText(AppConstants.RIDDLES_QUESTION_15);
                break;

            case "16":
                answer = AppConstants.RIDDLES_ANSwER_16;
                question.setText(AppConstants.RIDDLES_QUESTION_16);
                break;

            case "17":
                answer = AppConstants.RIDDLES_ANSwER_17;
                question.setText(AppConstants.RIDDLES_QUESTION_17);
                break;

            case "18":
                answer = AppConstants.RIDDLES_ANSwER_18;
                question.setText(AppConstants.RIDDLES_QUESTION_18);
                break;

            case "19":
                answer = AppConstants.RIDDLES_ANSwER_19;
                question.setText(AppConstants.RIDDLES_QUESTION_19);
                break;

            case "20":
                answer = AppConstants.RIDDLES_ANSwER_20;
                question.setText(AppConstants.RIDDLES_QUESTION_20);
                break;

            case "21":
                answer = AppConstants.RIDDLES_ANSwER_21;
                question.setText(AppConstants.RIDDLES_QUESTION_21);
                break;

            case "22":
                answer = AppConstants.RIDDLES_ANSwER_22;
                question.setText(AppConstants.RIDDLES_QUESTION_22);
                break;

            case "23":
                answer = AppConstants.RIDDLES_ANSwER_23;
                question.setText(AppConstants.RIDDLES_QUESTION_23);
                break;

            case "24":
                answer = AppConstants.RIDDLES_ANSwER_24;
                question.setText(AppConstants.RIDDLES_QUESTION_24);
                break;

            case "25":
                answer = AppConstants.RIDDLES_ANSwER_25;
                question.setText(AppConstants.RIDDLES_QUESTION_25);
                break;

            case "26":
                answer = AppConstants.RIDDLES_ANSwER_26;
                question.setText(AppConstants.RIDDLES_QUESTION_26);
                break;

            case "27":
                answer = AppConstants.RIDDLES_ANSwER_27;
                question.setText(AppConstants.RIDDLES_QUESTION_27);
                break;

            case "28":
                answer = AppConstants.RIDDLES_ANSwER_28;
                question.setText(AppConstants.RIDDLES_QUESTION_28);
                break;

            case "29":
                answer = AppConstants.RIDDLES_ANSwER_29;
                question.setText(AppConstants.RIDDLES_QUESTION_29);
                break;

            case "30":
                answer = AppConstants.RIDDLES_ANSwER_30;
                question.setText(AppConstants.RIDDLES_QUESTION_30);
                break;

            default:
                break;
        }
    }


    public void verifyAnswer() {
        switch (value) {
            case "1":
                if (editText_Answer.getText().toString().equals(answer)) {
                    showDialogCorrect(AppConstants.AWESOME);
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
//                        sendIntent();
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_1_PASSED, AppConstants.TRUE);
                    }
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "2":
                if (editText_Answer.getText().toString().equals(answer)) {
                    showDialogCorrect(AppConstants.AWESOME);
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_2_PASSED, AppConstants.TRUE);
                    }
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "3":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_3_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "4":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_4_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "5":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_5_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "6":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_6_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "7":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_7_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "8":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_8_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "9":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_9_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "10":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_10_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "11":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_11_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "12":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_12_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "13":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_13_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "14":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_14_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "15":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_15_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "16":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_16_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "17":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_17_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "18":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_18_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "19":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_19_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "20":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_20_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "21":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_21_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "22":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_22_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "23":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_23_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "24":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_24_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "25":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_25_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "26":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_26_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "27":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_27_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "28":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_28_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "29":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_29_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            case "30":
                if (editText_Answer.getText().toString().equals(answer)) {
                    if(game_type.equals( AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.RIDDLES_30_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                } else {
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;
            default:
                break;
        }
    }


    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }



    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.
        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_submit:
                btnClickSound();
                verifyAnswer();
                break;
            default:
                break;
        }
    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, "isExit");
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");
                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }

}
