package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class MathNumberTricksActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "MathNumberTricksActivit";
    Intent intent;
    String value, game_type;
    int score,numQuestions;
    Button btn_submit;
    LinearLayout line_answer_6, line_answer_5;
    EditText editTxt_answer_f,editTxt_answer_e, editTxt_answer_d, editTxt_answer_c, editTxt_answer_b, editTxt_answer_a;
    ImageView problem_img;
    MediaPlayer mp, mpWrong, mpCorrect;
    TextView timer;

    String ansA, ansB, ansC, ansD, ansE, ansF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_number_tricks);

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        getIntentData();
//        Toast.makeText(this, TAG + " " + value, Toast.LENGTH_SHORT).show();
        initUI();

        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }


    public void initUI(){
        timer = (TextView) findViewById(R.id.timer);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        line_answer_6 = (LinearLayout) findViewById(R.id.line_answer_6);
        line_answer_5 = (LinearLayout) findViewById(R.id.line_answer_5);
        editTxt_answer_a =(EditText) findViewById(R.id.editTxt_answer_a);
        editTxt_answer_b =(EditText) findViewById(R.id.editTxt_answer_b);
        editTxt_answer_c =(EditText) findViewById(R.id.editTxt_answer_c);
        editTxt_answer_d =(EditText) findViewById(R.id.editTxt_answer_d);
        editTxt_answer_e =(EditText) findViewById(R.id.editTxt_answer_e);
        editTxt_answer_f =(EditText) findViewById(R.id.editTxt_answer_f);
        problem_img = (ImageView) findViewById(R.id.problem_img);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score);
        refactorUI();
    }


    public void refactorUI(){
        switch (value){
            case "1":
                ansA = "8";
                ansB = "6";
                ansC = "13";
                ansD = "3";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_1);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "2":
                ansA = "7";
                ansB = "4";
                ansC = "11";
                ansD = "0";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_2);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "3":
                ansA = "12";
                ansB = "6";
                ansC = "5";
                ansD = "1";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_3);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "4":
                ansA = "14";
                ansB = "0";
                ansC = "0";
                ansD = "14";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_4);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "5":
                ansA = "9";
                ansB = "2";
                ansC = "15";
                ansD = "4";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_5);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "6":
                ansA = "1";
                ansB = "7";
                ansC = "9";
                ansD = "11";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_6);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "7":
                ansA = "8";
                ansB = "8";
                ansC = "9";
                ansD = "7";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_7);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "8":
                ansA = "6";
                ansB = "1";
                ansC = "7";
                ansD = "2";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_8);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "9":
                ansA = "5";
                ansB = "7";
                ansC = "6";
                ansD = "8";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_9);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "10":
                ansA = "3";
                ansB = "8";
                ansC = "15";
                ansD = "6";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_10);
                line_answer_5.setVisibility(View.GONE);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "11":
                ansA = "7";
                ansB = "4";
                ansC = "11";
                ansD = "6";
                ansE = "1";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_11);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "12":
                ansA = "16";
                ansB = "18";
                ansC = "15";
                ansD = "5";
                ansE = "7";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_12);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "13":
                ansA = "10";
                ansB = "27";
                ansC = "5";
                ansD = "12";
                ansE = "11";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_13);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "14":
                ansA = "20";
                ansB = "16";
                ansC = "7";
                ansD = "3";
                ansE = "8";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_14);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "15":
                ansA = "18";
                ansB = "14";
                ansC = "10";
                ansD = "1";
                ansE = "10";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_15);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "16":
                ansA = "18";
                ansB = "14";
                ansC = "10";
                ansD = "1";
                ansE = "48";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_16);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "17":
                ansA = "6";
                ansB = "9";
                ansC = "4";
                ansD = "2";
                ansE = "3";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_17);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "18":
                ansA = "17";
                ansB = "20";
                ansC = "4";
                ansD = "12";
                ansE = "0";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_18);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "19":
                ansA = "13";
                ansB = "30";
                ansC = "3";
                ansD = "14";
                ansE = "5";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_19);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "20":
                ansA = "14";
                ansB = "24";
                ansC = "3";
                ansD = "1";
                ansE = "1";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_20);
                line_answer_6.setVisibility(View.GONE);
                break;
            case "21":
                ansA = "12";
                ansB = "2";
                ansC = "8";
                ansD = "1";
                ansE = "1";
                ansF = "4";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_21);
                break;
            case "22":
                ansA = "21";
                ansB = "10";
                ansC = "10";
                ansD = "8";
                ansE = "4";
                ansF = "9";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_22);
                break;
            case "23":
                ansA = "22";
                ansB = "6";
                ansC = "8";
                ansD = "8";
                ansE = "4";
                ansF = "9";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_23);
                break;
            case "24":
                ansA = "25";
                ansB = "21";
                ansC = "18";
                ansD = "15";
                ansE = "5";
                ansF = "4";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_24);
                break;
            case "25":
                ansA = "18";
                ansB = "1";
                ansC = "9";
                ansD = "12";
                ansE = "12";
                ansF = "6";
                problem_img.setBackgroundResource(R.drawable.num_tricks_level_25);
                break;
            case "26":
                ansA = "6";
                ansB = "3";
                ansC = "6";
                ansD = "12";
                ansE = "15";
                ansF = "19";

                problem_img.setBackgroundResource(R.drawable.num_tricks_level_26);
                break;
            case "27":
                ansA = "28";
                ansB = "4";
                ansC = "4";
                ansD = "9";
                ansE = "21";
                ansF = "13";

                problem_img.setBackgroundResource(R.drawable.num_tricks_level_27);
                break;
            case "28":
                ansA = "8";
                ansB = "20";
                ansC = "19";
                ansD = "3";
                ansE = "1";
                ansF = "18";

                problem_img.setBackgroundResource(R.drawable.num_tricks_level_28);
                break;
            case "29":
                ansA = "24";
                ansB = "7";
                ansC = "23";
                ansD = "14";
                ansE = "15";
                ansF = "28";

                problem_img.setBackgroundResource(R.drawable.num_tricks_level_29);
                break;
            case "30":
                ansA = "4";
                ansB = "16";
                ansC = "5";
                ansD = "24";
                ansE = "2";
                ansF = "5";

                problem_img.setBackgroundResource(R.drawable.num_tricks_level_30);
                break;

            default:
                break;
        }
    }

    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.

        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.
    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_submit:
                btnClickSound();
                verifyAnswer();
                break;
            default:
                break;
        }
    }

    public void verifyAnswer(){

        switch(value){

            case "1":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_1_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "2":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_2_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "3":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_3_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "4":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_4_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "5":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_5_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "6":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_6_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "7":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_7_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "8":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_8_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "9":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_9_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "10":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_10_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "11":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_11_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "12":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_12_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "13":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_13_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "14":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_14_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "15":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_15_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "16":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_16_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "17":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_17_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "18":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_18_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "19":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_19_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "20":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_20_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "21":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_21_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "22":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_22_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "23":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_23_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "24":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_24_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "25":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_25_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "26":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_26_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "27":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_27_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "28":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_28_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "29":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_29_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "30":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)
                        &&editTxt_answer_d.getText().toString().equals(ansD) && editTxt_answer_e.getText().toString().equals(ansE) && editTxt_answer_f.getText().toString().equals(ansF)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.NUMTRICKS_30_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            default:
                break;
        }


    }

    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }
    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");
                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }

}
