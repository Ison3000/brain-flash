package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;
import java.util.Collections;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class LogicWhoIAmActivity extends AppCompatActivity {

    private static final String TAG = "LogicWhoIAmActivity";
    Intent intent;
    String value, game_type;
    int score, numQuestions;
    int id = 1;
    LinearLayout layout;
    TextView question, timer;
    MediaPlayer mp,mpCorrect, mpWrong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logic_who_iam);

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        getIntentData();
//        initUI();
    }


    public void initUI(){
        timer = (TextView) findViewById(R.id.timer);
        question = (TextView) findViewById(R.id.question);
        layout = (LinearLayout) findViewById(R.id.layout);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score);
        addButtons();
    }

    // Generates and returns a valid id that's not in use
    public int generateUniqueId(){
        View v = findViewById(id);
        while (v != null){
            v = findViewById(++id);
        }
        return id++;
    }

    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    public void dealWithButtonClick(Button b) {
        switch(b.getId()) {
            case 1:
                btnClickSound();
                switch (value){
                    case "1":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_1_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "2":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_2_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "3":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_3_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "4":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_4_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "5":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_5_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "6":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_6_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "7":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_7_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "8":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_8_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "9":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_9_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "10":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_10_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "11":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_11_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "12":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_12_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "13":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_13_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "14":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_14_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "15":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_15_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "16":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_16_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "17":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_17_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "18":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_18_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "19":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_19_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "20":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_20_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "21":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_21_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "22":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_22_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "23":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_23_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "24":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_24_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "25":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_25_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "26":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_26_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "27":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_27_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "28":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_28_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "29":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_29_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "30":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.CORRECT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.WHOIAM_30_PASSED, AppConstants.TRUE);
                        }
                        break;

                    default:
                        break;
                }

                showDialogCorrect(AppConstants.AWESOME);
                break;

            case 2:
                btnClickSound();
                showDialogIncorrect(AppConstants.TRY_AGAIN);
                break;
            default:
                break;
        }
    }


    public void addButtons(){

        ArrayList<Button> buttonList = new ArrayList<Button>();

        for (int i = 0; i < 2; i++) {
            final Button b = new Button(this);
            b.setGravity(Gravity.CENTER_HORIZONTAL);
            b.setId(generateUniqueId());                    // Set an id to Button
            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dealWithButtonClick(b);
                }
            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(50, 10, 50, 10);
            b.setLayoutParams(params);
            b.setBackgroundResource(R.drawable.blue_lining);
            b.setPadding(5, 5, 5, 5);
            b.setGravity(Gravity.CENTER);

            switch(b.getId()){
                case 1:
                    switch (value){
                        case "1":
                            question.setText(AppConstants.WHOIAM_QUESTION_1);
                            b.setText(AppConstants.WHOIAM_ANSwER_1);
//                            Prefs.putBoolean(AppConstants.WHOIAM_1_PASSED, AppConstants.TRUE);
                            Log.e(TAG, "addButtons: " + Prefs.getBoolean(AppConstants.NUMTRICKS_1_PASSED, AppConstants.TRUE ));
                            break;
                        case "2":
                            question.setText(AppConstants.WHOIAM_QUESTION_2);
                            b.setText(AppConstants.WHOIAM_ANSwER_2);
//                            Prefs.putBoolean(AppConstants.WHOIAM_2_PASSED, AppConstants.TRUE);
                            break;
                        case "3":
                            question.setText(AppConstants.WHOIAM_QUESTION_3);
                            b.setText(AppConstants.WHOIAM_ANSwER_3);
//                            Prefs.putBoolean(AppConstants.WHOIAM_3_PASSED, AppConstants.TRUE);
                            break;
                        case "4":
                            question.setText(AppConstants.WHOIAM_QUESTION_4);
                            b.setText(AppConstants.WHOIAM_ANSwER_4);
//                            Prefs.putBoolean(AppConstants.WHOIAM_4_PASSED, AppConstants.TRUE);
                            break;
                        case "5":
                            question.setText(AppConstants.WHOIAM_QUESTION_5);
                            b.setText(AppConstants.WHOIAM_ANSwER_5);
//                            Prefs.putBoolean(AppConstants.WHOIAM_5_PASSED, AppConstants.TRUE);
                            break;
                        case "6":
                            question.setText(AppConstants.WHOIAM_QUESTION_6);
                            b.setText(AppConstants.WHOIAM_ANSwER_6);
//                            Prefs.putBoolean(AppConstants.WHOIAM_6_PASSED, AppConstants.TRUE);
                            break;

                        case "7":
                            question.setText(AppConstants.WHOIAM_QUESTION_7);
                            b.setText(AppConstants.WHOIAM_ANSwER_7);
//                            Prefs.putBoolean(AppConstants.WHOIAM_7_PASSED, AppConstants.TRUE);
                            break;
                        case "8":
                            question.setText(AppConstants.WHOIAM_QUESTION_8);
                            b.setText(AppConstants.WHOIAM_ANSwER_8);
//                            Prefs.putBoolean(AppConstants.WHOIAM_8_PASSED, AppConstants.TRUE);
                            break;
                        case "9":
                            question.setText(AppConstants.WHOIAM_QUESTION_9);
                            b.setText(AppConstants.WHOIAM_ANSwER_9);
//                            Prefs.putBoolean(AppConstants.WHOIAM_9_PASSED, AppConstants.TRUE);
                            break;
                        case "10":
                            question.setText(AppConstants.WHOIAM_QUESTION_10);
                            b.setText(AppConstants.WHOIAM_ANSwER_10);
//                            Prefs.putBoolean(AppConstants.WHOIAM_10_PASSED, AppConstants.TRUE);
                            break;
                        case "11":
                            question.setText(AppConstants.WHOIAM_QUESTION_11);
                            b.setText(AppConstants.WHOIAM_ANSwER_11);
//                            Prefs.putBoolean(AppConstants.WHOIAM_11_PASSED, AppConstants.TRUE);
                            break;
                        case "12":
                            question.setText(AppConstants.WHOIAM_QUESTION_12);
                            b.setText(AppConstants.WHOIAM_ANSwER_12);
//                            Prefs.putBoolean(AppConstants.WHOIAM_12_PASSED, AppConstants.TRUE);
                            break;

                        case "13":
                            question.setText(AppConstants.WHOIAM_QUESTION_13);
                            b.setText(AppConstants.WHOIAM_ANSwER_13);
//                            Prefs.putBoolean(AppConstants.WHOIAM_13_PASSED, AppConstants.TRUE);
                            break;
                        case "14":
                            question.setText(AppConstants.WHOIAM_QUESTION_14);
                            b.setText(AppConstants.WHOIAM_ANSwER_14);
//                            Prefs.putBoolean(AppConstants.WHOIAM_14_PASSED, AppConstants.TRUE);
                            break;
                        case "15":
                            question.setText(AppConstants.WHOIAM_QUESTION_15);
                            b.setText(AppConstants.WHOIAM_ANSwER_15);
//                            Prefs.putBoolean(AppConstants.WHOIAM_15_PASSED, AppConstants.TRUE);
                            break;
                        case "16":
                            question.setText(AppConstants.WHOIAM_QUESTION_16);
                            b.setText(AppConstants.WHOIAM_ANSwER_16);
//                            Prefs.putBoolean(AppConstants.WHOIAM_16_PASSED, AppConstants.TRUE);
                            break;
                        case "17":
                            question.setText(AppConstants.WHOIAM_QUESTION_17);
                            b.setText(AppConstants.WHOIAM_ANSwER_17);
//                            Prefs.putBoolean(AppConstants.WHOIAM_17_PASSED, AppConstants.TRUE);
                            break;
                        case "18":
                            question.setText(AppConstants.WHOIAM_QUESTION_18);
                            b.setText(AppConstants.WHOIAM_ANSwER_18);
//                            Prefs.putBoolean(AppConstants.WHOIAM_18_PASSED, AppConstants.TRUE);
                            break;

                        case "19":
                            question.setText(AppConstants.WHOIAM_QUESTION_19);
                            b.setText(AppConstants.WHOIAM_ANSwER_19);
//                            Prefs.putBoolean(AppConstants.WHOIAM_19_PASSED, AppConstants.TRUE);
                            break;
                        case "20":
                            question.setText(AppConstants.WHOIAM_QUESTION_20);
                            b.setText(AppConstants.WHOIAM_ANSwER_20);
//                            Prefs.putBoolean(AppConstants.WHOIAM_20_PASSED, AppConstants.TRUE);
                            break;
                        case "21":
                            question.setText(AppConstants.WHOIAM_QUESTION_21);
//                            Prefs.putBoolean(AppConstants.WHOIAM_21_PASSED, AppConstants.TRUE);
                            b.setText(AppConstants.WHOIAM_ANSwER_21);
                            break;
                        case "22":
                            question.setText(AppConstants.WHOIAM_QUESTION_22);
                            b.setText(AppConstants.WHOIAM_ANSwER_22);
//                            Prefs.putBoolean(AppConstants.WHOIAM_22_PASSED, AppConstants.TRUE);
                            break;
                        case "23":
                            question.setText(AppConstants.WHOIAM_QUESTION_23);
                            b.setText(AppConstants.WHOIAM_ANSwER_23);
//                            Prefs.putBoolean(AppConstants.WHOIAM_23_PASSED, AppConstants.TRUE);
                            break;
                        case "24":
                            question.setText(AppConstants.WHOIAM_QUESTION_24);
                            b.setText(AppConstants.WHOIAM_ANSwER_24);
//                            Prefs.putBoolean(AppConstants.WHOIAM_24_PASSED, AppConstants.TRUE);
                            break;

                        case "25":
                            question.setText(AppConstants.WHOIAM_QUESTION_25);
                            b.setText(AppConstants.WHOIAM_ANSwER_25);
//                            Prefs.putBoolean(AppConstants.WHOIAM_25_PASSED, AppConstants.TRUE);
                            break;
                        case "26":
                            question.setText(AppConstants.WHOIAM_QUESTION_26);
                            b.setText(AppConstants.WHOIAM_ANSwER_26);
//                            Prefs.putBoolean(AppConstants.WHOIAM_26_PASSED, AppConstants.TRUE);
                            break;
                        case "27":
                            question.setText(AppConstants.WHOIAM_QUESTION_27);
                            b.setText(AppConstants.WHOIAM_ANSwER_27);
//                            Prefs.putBoolean(AppConstants.WHOIAM_27_PASSED, AppConstants.TRUE);
                            break;
                        case "28":
                            question.setText(AppConstants.WHOIAM_QUESTION_28);
                            b.setText(AppConstants.WHOIAM_ANSwER_28);
//                            Prefs.putBoolean(AppConstants.WHOIAM_28_PASSED, AppConstants.TRUE);
                            break;
                        case "29":
                            question.setText(AppConstants.WHOIAM_QUESTION_29);
                            b.setText(AppConstants.WHOIAM_ANSwER_29);
//                            Prefs.putBoolean(AppConstants.WHOIAM_29_PASSED, AppConstants.TRUE);
                            break;
                        case "30":
                            question.setText(AppConstants.WHOIAM_QUESTION_30);
                            b.setText(AppConstants.WHOIAM_ANSwER_30);
//                            Prefs.putBoolean(AppConstants.WHOIAM_30_PASSED, AppConstants.TRUE);
                            break;

                        default:
                            break;
                    }
                    break;
                case 2:
                    switch (value){
                        case "1":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_1);
                            break;
                        case "2":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_2);
                            break;
                        case "3":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_3);
                            break;
                        case "4":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_4);
                            break;
                        case "5":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_5);
                            break;
                        case "6":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_6);
                            break;

                        case "7":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_7);
                            break;
                        case "8":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_8);
                            break;
                        case "9":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_9);
                            break;
                        case "10":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_10);
                            break;
                        case "11":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_11);
                            break;
                        case "12":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_12);
                            break;

                        case "13":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_13);
                            break;
                        case "14":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_14);
                            break;
                        case "15":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_15);
                            break;
                        case "16":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_16);
                            break;
                        case "17":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_17);
                            break;
                        case "18":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_18);
                            break;

                        case "19":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_19);
                            break;
                        case "20":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_20);
                            break;
                        case "21":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_21);
                            break;
                        case "22":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_22);
                            break;
                        case "23":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_23);
                            break;
                        case "24":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_24);
                            break;

                        case "25":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_25);
                            break;
                        case "26":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_26);
                            break;
                        case "27":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_27);
                            break;
                        case "28":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_28);
                            break;
                        case "29":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_29);
                            break;
                        case "30":
                            b.setText(AppConstants.WHOIAM_ANSwER_INCORRECT_30);
                            break;

                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            buttonList.add(b);
        }

// Shuffle
        Collections.shuffle(buttonList);
        for (int i = 0; i < 2; i++) {
            if (i < 3) {
                layout.addView(buttonList.get(i));
            } else {
                //Do Nothing
            }
        }

    }

    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }

    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.

        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
        }

        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");
                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }
}
