package ison.projects.com.brainflash.Fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import ison.projects.com.brainflash.Activities.LevelSelectActivity;
import ison.projects.com.brainflash.Activities.LogicRiddlesActivity;
import ison.projects.com.brainflash.Activities.LogicSolutionsActivity;
import ison.projects.com.brainflash.Activities.LogicWhoIAmActivity;
import ison.projects.com.brainflash.Activities.MAthOperationActivity;
import ison.projects.com.brainflash.Activities.MAthPatternRecognitionActivity;
import ison.projects.com.brainflash.Activities.MathNumberTricksActivity;
import ison.projects.com.brainflash.Activities.MemoryBoxOWordsActivity;
import ison.projects.com.brainflash.Activities.MemorySyntaxActivity;
import ison.projects.com.brainflash.Activities.MemoryWordQuizActivity;
import ison.projects.com.brainflash.Activities.TrainingActvity;
import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;


public class TrainingFragment extends Fragment implements View.OnClickListener {

    private View mView;
    Button btn_start;
    Intent myIntent;
    String name;
    TextView textview_name;

    String formattedDate, currenDate;
    SimpleDateFormat df;
    Date c;

    Class activity_dest;
    Random rand;
    int n;


    private static final String TAG = "TrainingFragment";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_training, container, false);

            initUI();
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        initUI();
    }

    public void initUI(){
        Prefs.getBoolean(AppConstants.IS_FIRST_RUN, AppConstants.TRUE);
        currenDate = getCurrentDate();
        btn_start = (Button) mView.findViewById(R.id.btn_start);
        btn_start.setOnClickListener(this);
        textview_name = (TextView) mView.findViewById(R.id.textview_name);
        textview_name.setText(AppConstants.HELLO + Prefs.getString(AppConstants.NAME, AppConstants.PLAYER));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_start:
                btnClickSound();
                sendIntent();
                break;

            default:
                break;
        }
    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(getActivity(), R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    public void sendIntent(){
        myIntent = new Intent(getActivity(), TrainingActvity.class);
        if(  Prefs.getBoolean(AppConstants.IS_FIRST_RUN, AppConstants.TRUE)){
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, false);
            Prefs.putBoolean(AppConstants.IS_FIRST_RUN, false);
        }
        else{
            if(getCurrentDate().equals(Prefs.getString(AppConstants.CURRENT_DATE, currenDate))){
                Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            }
            else{
                Prefs.putBoolean(AppConstants.IS_ACTIVITY, false);
            }
        }

        getActivity().startActivity(myIntent);
    }

    public String getCurrentDate(){
        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);
//        Prefs.putString(AppConstants.CURRENT_DATE, formattedDate);
        Log.e(TAG, "getCurrentDate: " + formattedDate);
        return formattedDate.toString();
    }


    public int generateNumber(int limit){
        rand = new Random();

        n = rand.nextInt(limit);

        n += 1;
        Log.e(TAG, "generateNumber: " + n );
        return n;
    }

    public void setDestination(){
        switch (generateNumber(9)){
            case 1:
                activity_dest = MathNumberTricksActivity.class;
                break;

            case 2:
                activity_dest = MAthOperationActivity.class;
                break;

            case 3:
                activity_dest = MAthPatternRecognitionActivity.class;
                break;

            case 4:
                activity_dest = LogicRiddlesActivity.class;
                break;

            case 5:
                activity_dest = LogicSolutionsActivity.class;
                break;

            case 6:
                activity_dest = LogicWhoIAmActivity.class;
                break;

            case 7:
                activity_dest = MemoryWordQuizActivity.class;
                break;

            case 8:
                activity_dest = MemoryBoxOWordsActivity.class;
                break;

            case 9:
                activity_dest = MemorySyntaxActivity.class;
                break;

            default:
                break;
        }
    }




}
