package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;
import java.util.Collections;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class MemorySyntaxActivity extends AppCompatActivity {

    private static final String TAG = "MemorySyntaxActivity";
    Intent intent;
    String value, game_type;
    int score, numQuestions;
    int id = 1;
    LinearLayout layout;
    TextView question, timer;
    MediaPlayer mp, mpWrong, mpCorrect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syntax);
        getIntentData();

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score);
//        initUI();
    }


    public void initUI(){
        timer = (TextView) findViewById(R.id.timer);
        question = (TextView) findViewById(R.id.question);
        layout = (LinearLayout) findViewById(R.id.layout);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        addButtons();

        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }

    // Generates and returns a valid id that's not in use
    public int generateUniqueId(){
        View v = findViewById(id);
        while (v != null){
            v = findViewById(++id);
        }
        return id++;
    }


    public void dealWithButtonClick(Button b) {
        switch(b.getId()) {
            case 1:
                switch (value){
                    case "1":
                        btnClickSound();
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_1_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "2":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_2_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "3":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_3_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "4":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_4_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "5":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_5_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "6":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_6_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "7":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_7_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "8":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_8_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "9":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_9_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "10":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_10_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "11":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_11_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "12":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_12_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "13":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_13_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "14":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_14_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "15":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_15_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "16":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_16_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "17":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_17_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "18":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_18_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "19":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_19_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "20":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_20_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "21":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_21_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "22":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_22_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "23":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_23_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "24":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_24_PASSED, AppConstants.TRUE);
                        }
                        break;

                    case "25":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_25_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "26":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_26_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "27":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_27_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "28":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_28_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "29":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_29_PASSED, AppConstants.TRUE);
                        }
                        break;
                    case "30":
                        if(game_type.equals(AppConstants.TRAINING)){
                            Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                            Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        }
                        else{
                            Prefs.putBoolean(AppConstants.SYNTAX_30_PASSED, AppConstants.TRUE);
                        }
                        break;

                    default:
                        break;
                }
                showDialogCorrect(AppConstants.AWESOME);
                break;

            case 2:
                btnClickSound();
                showDialogIncorrect(AppConstants.TRY_AGAIN);
                break;
            default:
                break;
        }
    }



    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    public void addButtons(){

        ArrayList<Button> buttonList = new ArrayList<Button>();

        for (int i = 0; i < 2; i++) {
            final Button b = new Button(this);
            b.setGravity(Gravity.CENTER_HORIZONTAL);
            b.setId(generateUniqueId());                    // Set an id to Button
            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dealWithButtonClick(b);
                }
            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(50, 10, 50, 10);
            b.setLayoutParams(params);
            b.setPadding(50, 10, 50, 10);
            b.setGravity(Gravity.CENTER);

            switch(b.getId()){
                case 1:
                    switch (value){
                        case "1":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_1));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "2":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_2));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "3":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_3));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "4":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_4));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "5":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_5));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "6":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_6));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;

                        case "7":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_7));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "8":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_8));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "9":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_9));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "10":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_10));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "11":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_11));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "12":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_12));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;

                        case "13":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_13));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "14":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_14));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "15":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_15));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "16":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_16));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "17":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_17));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "18":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_18));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;

                        case "19":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_19));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "20":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_20));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "21":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_21));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "22":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_22));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "23":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_23));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "24":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_24));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;

                        case "25":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_25));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "26":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_26));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "27":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_27));
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "28":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_28));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "29":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_29));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "30":
                            question.setText(Html.fromHtml(AppConstants.SYNTAX_QUESTION_30));
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;

                        default:
                            break;
                    }
                    break;
                case 2:
                    switch (value){
                        case "1":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "2":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "3":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "4":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "5":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "6":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;

                        case "7":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "8":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "9":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "10":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "11":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "12":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;

                        case "13":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "14":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "15":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "16":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "17":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "18":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;

                        case "19":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "20":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "21":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "22":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "23":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "24":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;

                        case "25":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "26":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "27":
                            b.setBackgroundResource(R.drawable.btn_cross);
                            break;
                        case "28":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "29":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;
                        case "30":
                            b.setBackgroundResource(R.drawable.btn_check);
                            break;

                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            buttonList.add(b);
        }

// Shuffle
        Collections.shuffle(buttonList);
        for (int a = 0; a < 2; a++) {
            if (a < 2) {
                layout.addView(buttonList.get(a));
            } else {
                //Do Nothing
            }
        }

    }
    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }


    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.
        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.

    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");

                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }
}
