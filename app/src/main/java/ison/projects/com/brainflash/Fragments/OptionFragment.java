package ison.projects.com.brainflash.Fragments;


import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import ison.projects.com.brainflash.Activities.AboutActivity;
import ison.projects.com.brainflash.Activities.HelpActivity;
import ison.projects.com.brainflash.Activities.MainActivity;
import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OptionFragment extends Fragment implements View.OnClickListener{

    private View mView;

    EditText editText_player_name;
    Button btn_save, btn_soundfx, btn_music, btn_help, btn_option;
    Intent intent;

    public OptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_option, container, false);
        Log.e("TAG", "onClick: " +  Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE));
        initUI();
        return mView;
    }

    public void initUI(){
        btn_option = (Button) mView.findViewById(R.id.btn_about);
        btn_option.setOnClickListener(this);
        btn_help = (Button) mView.findViewById(R.id.btn_help);
        btn_help.setOnClickListener(this);
        editText_player_name = (EditText) mView. findViewById(R.id.edittext_player_name);
        editText_player_name.setText(Prefs.getString(AppConstants.NAME, AppConstants.PLAYER));
        btn_save = mView.findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);
        btn_music = (Button) mView.findViewById(R.id.btn_music);
        btn_music.setOnClickListener(this);
        if( Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            btn_music.setBackgroundResource(R.drawable.sound_on);
        }
        else{
            btn_music.setBackgroundResource(R.drawable.sound_off);
        }
        btn_soundfx = (Button) mView.findViewById(R.id.btn_soundfx);
        btn_soundfx.setOnClickListener(this);
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            btn_soundfx.setBackgroundResource(R.drawable.sound_on);
        }
        else{
            btn_soundfx.setBackgroundResource(R.drawable.sound_off);
        }

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_save:
                Prefs.putString(AppConstants.NAME, editText_player_name.getText().toString());
                showDialogCorrect("Saved");
                break;

            case R.id.btn_soundfx:
                if(Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
                    btn_soundfx.setBackgroundResource(R.drawable.sound_off);
                    Prefs.putBoolean(AppConstants.IS_SOUND_ON, AppConstants.FALSE);

                }
                else {
                    btn_soundfx.setBackgroundResource(R.drawable.sound_on);
                    Prefs.putBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE);
                }
                break;

            case R.id.btn_music:
                if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
                    btn_music.setBackgroundResource(R.drawable.sound_off);
                    Prefs.putBoolean(AppConstants.IS_MUSIC_ON, AppConstants.FALSE);
                    ((MainActivity)getActivity()).mpStop();
                }
                else {
                    btn_music.setBackgroundResource(R.drawable.sound_on);
                    Prefs.putBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE);
                    ((MainActivity)getActivity()).mpStart();
                }
                break;

            case R.id.btn_about:
                intent = new Intent(getActivity(), AboutActivity.class);
                this.startActivity(intent);
                break;

            case R.id.btn_help:
                intent = new Intent(getActivity(), HelpActivity.class);
                this.startActivity(intent);
                break;

            default:
                break;
        }
    }


    public void showDialogCorrect(String result) {

        new LovelyStandardDialog(getActivity(), LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        }
                })
                .show();
    }
}
