
package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.pixplicity.easyprefs.library.Prefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class TrainingActvity extends AppCompatActivity  implements View.OnClickListener{

    Intent intent;
    String value;

    String formattedDate;
    SimpleDateFormat df;
    Date c;
    Class activity_dest;
    Random rand;
    String currentDate;

    boolean isBackPressed;
    int n, score = 0, question_count, count, wrong, numQuestions;

    private static final String TAG = "TrainingActvity";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_actvity);


        init();
    }

    public void init(){
        currentDate = getCurrentDate();
        count = Prefs.getInt(AppConstants.START_DAY, 0);
        Log.e(TAG, "init: "+ Prefs.getString(AppConstants.CURRENT_DATE, "today"));
        Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, false);
    }

    public void startTraining(){

        setDestination();
        sendIntent(generateNumber(30), question_count);
//        sendIntent(generateNumber(30), question_count);
    }



    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume: value : " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, false ));
        if(Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, false) == false){
            Log.e(TAG, "onResume1: " + true );
            if(Prefs.getBoolean(AppConstants.IS_ACTIVITY, false)){

            }
            else{
                Log.e(TAG, "onResume2: " + true );
                question_count =  Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, score);
                Log.e(TAG, "onResume: " + question_count + " "+ numQuestions);

                numQuestions =  Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);

                if(currentDate.equals(Prefs.getString(AppConstants.CURRENT_DATE, "today"))){
                    Log.e(TAG, "onResume: " + currentDate.equals(Prefs.getString(AppConstants.CURRENT_DATE, "today")) );
//                    checkDailyQuestions();
                    Log.e(TAG, "onResume:3 " + true );
                    if(question_count < AppConstants.QUESTION_TARGET){
                        startTraining();
                    }
                    else{
                        Log.e(TAG, "onResume:4 " + false );
                        Prefs.putString(AppConstants.CURRENT_DATE, getCurrentDate());
                        setAnswerData(question_count);
                    }
                }
                else{
                    Log.e(TAG, "onResume:5 " + false );
                    Prefs.putString(AppConstants.CURRENT_DATE, currentDate);
                    Prefs.putInt(AppConstants.START_DAY, count + 1 );
                    switch (Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, score)){

                        case 1:
                            Prefs.putInt(AppConstants.SCORE_1, question_count);
                            break;
                        case 2:
                            Prefs.putInt(AppConstants.SCORE_2, question_count);
                            break;
                        case 3:
                            Prefs.putInt(AppConstants.SCORE_3, question_count);
                            break;
                        case 4:
                            Prefs.putInt(AppConstants.SCORE_4, question_count);
                            break;
                        case 5:
                            Prefs.putInt(AppConstants.SCORE_5, question_count);
                            break;
                        case 6:
                            Prefs.putInt(AppConstants.SCORE_6, question_count);
                            break;
                        case 7:
                            Prefs.putInt(AppConstants.SCORE_7, question_count);
                            break;
                        case 8:
                            Prefs.putInt(AppConstants.SCORE_8, question_count);
                            break;
                        case 9:
                            Prefs.putInt(AppConstants.SCORE_9, question_count);
                            break;
                        case 10:
                            Prefs.putInt(AppConstants.SCORE_10, question_count);
                            break;
                        case 11:
                            Prefs.putInt(AppConstants.SCORE_11, question_count);
                            break;
                        case 12:
                            Prefs.putInt(AppConstants.SCORE_12, question_count);
                            break;
                        case 13:
                            Prefs.putInt(AppConstants.SCORE_13, question_count);
                            break;
                        case 14:
                            Prefs.putInt(AppConstants.SCORE_14, question_count);
                            break;
                        case 15:
                            Prefs.putInt(AppConstants.SCORE_15, question_count);
                            break;
                        case 16:
                            Prefs.putInt(AppConstants.SCORE_16, question_count);
                            break;
                        case 17:
                            Prefs.putInt(AppConstants.SCORE_17, question_count);
                            break;
                        case 18:
                            Prefs.putInt(AppConstants.SCORE_18, question_count);
                            break;
                        case 19:
                            Prefs.putInt(AppConstants.SCORE_19, question_count);
                            break;
                        case 20:
                            Prefs.putInt(AppConstants.SCORE_20, question_count);
                            break;
                        case 21:
                            Prefs.putInt(AppConstants.SCORE_21, question_count);
                            break;
                        case 22:
                            Prefs.putInt(AppConstants.SCORE_22, question_count);
                            break;
                        case 23:
                            Prefs.putInt(AppConstants.SCORE_23, question_count);
                            break;
                        case 24:
                            Prefs.putInt(AppConstants.SCORE_24, question_count);
                            break;
                        case 25:
                            Prefs.putInt(AppConstants.SCORE_25, question_count);
                            break;
                        case 26:
                            Prefs.putInt(AppConstants.SCORE_26, question_count);
                            break;
                        case 27:
                            Prefs.putInt(AppConstants.SCORE_27, question_count);
                            break;
                        case 28:
                            Prefs.putInt(AppConstants.SCORE_28, question_count);
                            break;
                        case 29:
                            Prefs.putInt(AppConstants.SCORE_29, question_count);
                            break;
                        case 30:
                            Prefs.putInt(AppConstants.SCORE_30, question_count);
                            break;

                        default:
                            break;
                    }

                    Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score);
                    Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions);

                    checkDailyQuestions();
                }
            }
        }
        else{
            setAnswerData(Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, question_count));
            Log.e(TAG, "onResume6: " + question_count );
            Log.e(TAG, "onResume6: " + false );
            //Do Nothing
        }


//        setDestination();
//        sendIntent(generateNumber(30));
    }

    public void setAnswerData(int scoreOfDay){
        switch(Prefs.getInt(AppConstants.START_DAY, 0)){
            case 1:
                Prefs.putInt(AppConstants.SCORE_1, scoreOfDay );
                Log.e(TAG, "setAnswerData1: " +   Prefs.getInt(AppConstants.SCORE_1, scoreOfDay ));
                break;

            case 2:
                Prefs.putInt(AppConstants.SCORE_2, scoreOfDay );
                Log.e(TAG, "setAnswerData2: " +   Prefs.getInt(AppConstants.SCORE_2, scoreOfDay ));
                break;

            case 3:
                Prefs.putInt(AppConstants.SCORE_3, scoreOfDay );
                break;

            case 4:
                Prefs.putInt(AppConstants.SCORE_4, scoreOfDay );
                break;

            case 5:
                Prefs.putInt(AppConstants.SCORE_5, scoreOfDay );
                break;

            case 6:
                Prefs.putInt(AppConstants.SCORE_6, scoreOfDay );
                break;

            case 7:
                Prefs.putInt(AppConstants.SCORE_7, scoreOfDay );
                break;

            case 8:
                Prefs.putInt(AppConstants.SCORE_8, scoreOfDay );
                break;

            case 9:
                Prefs.putInt(AppConstants.SCORE_9, scoreOfDay );
                break;

            case 10:
                Prefs.putInt(AppConstants.SCORE_10, scoreOfDay );
                break;

            case 11:
                Prefs.putInt(AppConstants.SCORE_11, scoreOfDay );
                break;

            case 12:
                Prefs.putInt(AppConstants.SCORE_12, scoreOfDay );
                break;

            case 13:
                Prefs.putInt(AppConstants.SCORE_13, scoreOfDay );
                break;

            case 14:
                Prefs.putInt(AppConstants.SCORE_14, scoreOfDay );
                break;

            case 15:
                Prefs.putInt(AppConstants.SCORE_15, scoreOfDay );
                break;

            case 16:
                Prefs.putInt(AppConstants.SCORE_16, scoreOfDay );
                break;

            case 17:
                Prefs.putInt(AppConstants.SCORE_17, scoreOfDay );
                break;

            case 18:
                Prefs.putInt(AppConstants.SCORE_18, scoreOfDay );
                break;

            case 19:
                Prefs.putInt(AppConstants.SCORE_19, scoreOfDay );
                break;

            case 20:
                Prefs.putInt(AppConstants.SCORE_20, scoreOfDay );
                break;

            case 21:
                Prefs.putInt(AppConstants.SCORE_21, scoreOfDay );
                break;

            case 22:
                Prefs.putInt(AppConstants.SCORE_22, scoreOfDay );
                break;

            case 23:
                Prefs.putInt(AppConstants.SCORE_23, scoreOfDay );
                break;

            case 24:
                Prefs.putInt(AppConstants.SCORE_24, scoreOfDay );
                break;

            case 25:
                Prefs.putInt(AppConstants.SCORE_25, scoreOfDay );
                break;

            case 26:
                Prefs.putInt(AppConstants.SCORE_26, scoreOfDay );
                break;

            case 27:
                Prefs.putInt(AppConstants.SCORE_27, scoreOfDay );
                break;

            case 28:
                Prefs.putInt(AppConstants.SCORE_28, scoreOfDay );
                break;

            case 29:
                Prefs.putInt(AppConstants.SCORE_29, scoreOfDay );
                break;

            case 30:
                Prefs.putInt(AppConstants.SCORE_30, scoreOfDay );
                break;

            default:
                break;
        }
    }


    public void checkDailyQuestions(){
        if(numQuestions <= 20){
            if(question_count < 20){
                startTraining();
            }
            else{
                Prefs.putString(AppConstants.CURRENT_DATE, getCurrentDate());
            }
        }
        else
        {
            Prefs.putString(AppConstants.CURRENT_DATE, getCurrentDate());
        }
    }

    public void sendIntent(int level, int count){
        intent = new Intent(this, activity_dest);
//        intent = new Intent(this, MemoryWordQuizActivity.class);
        intent.putExtra(AppConstants.GAME_TYPE, AppConstants.TRAINING);
        intent.putExtra(AppConstants.LEVEL,  String.valueOf(level)); //Optional parameters
        this.startActivity(intent);
    }

    public String getCurrentDate(){
        c = Calendar.getInstance().getTime();
        df = new SimpleDateFormat("dd-MMM-yyyy");
        formattedDate = df.format(c);
//        Prefs.putString(AppConstants.CURRENT_DATE, formattedDate);
        Log.e(TAG, "getCurrentDate: " + formattedDate);
        return formattedDate.toString();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            default:
                break;
        }
    }

    public int generateNumber(int limit){
        rand = new Random();

        n = rand.nextInt(limit);

        n += 1;
        return n;
    }

    public void setDestination(){
        switch (generateNumber(9)){
            case 1:
                activity_dest = MathNumberTricksActivity.class;
                break;

            case 2:
                activity_dest = MAthOperationActivity.class;
                break;

            case 3:
                activity_dest = MAthPatternRecognitionActivity.class;
                break;

            case 4:
                activity_dest = LogicRiddlesActivity.class;
                break;

            case 5:
                activity_dest = LogicSolutionsActivity.class;
                break;

            case 6:
                activity_dest = LogicWhoIAmActivity.class;
                break;

            case 7:
                activity_dest = MemoryWordQuizActivity.class;
                break;

            case 8:
                activity_dest = MemoryBoxOWordsActivity.class;
                break;

            case 9:
                activity_dest = MemorySyntaxActivity.class;
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}
