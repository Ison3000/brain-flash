package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class LogicSolutionsActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "LogicSolutionsActivity";
    Intent intent;
    String value, game_type;
    int score, numQuestions;
    Button btn_submit;
    LinearLayout line_answer_3;
    EditText editTxt_answer_c, editTxt_answer_b, editTxt_answer_a;
    ImageView problem_img;
    MediaPlayer mp, mpCorrect, mpWrong;
    String ansA, ansB, ansC;
    TextView timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logic_solutions);

        getIntentData();
//        Toast.makeText(this, TAG + " " + value, Toast.LENGTH_SHORT).show();
        initUI();

        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }


    public void initUI(){
        timer = (TextView) findViewById(R.id.timer);
        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        line_answer_3 = (LinearLayout) findViewById(R.id.line_answer_3);
        editTxt_answer_a =(EditText) findViewById(R.id.editTxt_answer_a);
        editTxt_answer_b =(EditText) findViewById(R.id.editTxt_answer_b);
        editTxt_answer_c =(EditText) findViewById(R.id.editTxt_answer_c);
        problem_img = (ImageView) findViewById(R.id.problem_img);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        Log.e(TAG, "initUI: " + score);
        refactorUI();
    }


    public void refactorUI(){
        switch (value){
            case "1":
                ansA = "2";
                ansB = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl1);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "2":
                ansA = "5";
                ansB = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl2);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "3":
                ansA = "5";
                ansB = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl3);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "4":
                ansA = "1";
                ansB = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl4);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "5":
                ansA = "3";
                ansB = "5";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl5);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "6":
                ansA = "1";
                ansB = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl6);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "7":
                ansA = "1";
                ansB = "5";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl7);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "8":
                ansA = "6";
                ansB = "5";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl8);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "9":
                ansA = "4";
                ansB = "2";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl9);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "10":
                ansA = "1";
                ansB = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl10);
                line_answer_3.setVisibility(View.GONE);
                break;
            case "11":
                ansA = "5";
                ansB = "4";
                ansC = "0";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl11);
                break;
            case "12":
                ansA = "1";
                ansB = "0";
                ansC = "3";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl12);
                break;
            case "13":
                ansA = "8";
                ansB = "4";
                ansC = "5";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl13);
                break;
            case "14":
                ansA = "6";
                ansB = "0";
                ansC = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl14);
                break;
            case "15":
                ansA = "4";
                ansB = "3";
                ansC = "2";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl15);
                break;
            case "16":
                ansA = "9";
                ansB = "5";
                ansC = "5";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl16);
                break;
            case "17":
                ansA = "5";
                ansB = "8";
                ansC = "3";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl17);
                break;
            case "18":
                ansA = "8";
                ansB = "9";
                ansC = "5";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl18);
                break;
            case "19":
                ansA = "3";
                ansB = "2";
                ansC = "2";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl19);
                break;
            case "20":
                ansA = "4";
                ansB = "3";
                ansC = "2";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl20);
                break;
            case "21":
                ansA = "2";
                ansB = "3";
                ansC = "8";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl21);
                break;
            case "22":
                ansA = "4";
                ansB = "1";
                ansC = "0";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl22);
                break;
            case "23":
                ansA = "3";
                ansB = "3";
                ansC = "1";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl23);
                break;
            case "24":
                ansA = "5";
                ansB = "0";
                ansC = "7";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl24);
                break;
            case "25":
                ansA = "5";
                ansB = "4";
                ansC = "9";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl25);
                break;
            case "26":
                ansA = "5";
                ansB = "5";
                ansC = "7";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl26);
                break;
            case "27":
                ansA = "7";
                ansB = "3";
                ansC = "7";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl27);
                break;
            case "28":
                ansA = "8";
                ansB = "0";
                ansC = "9";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl28);
                break;
            case "29":
                ansA = "3";
                ansB = "3";
                ansC = "2";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl29);
                break;
            case "30":
                ansA = "6";
                ansB = "2";
                ansC = "0";
                problem_img.setBackgroundResource(R.drawable.sologic_lvl30);
                break;

            default:
                break;
        }
    }

    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.

        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_submit:
                btnClickSound();
                verifyAnswer();
                break;
            default:
                break;
        }
    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    public void verifyAnswer(){

        switch(value){

            case "1":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_1_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "2":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_2_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "3":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_3_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "4":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_4_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "5":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_5_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "6":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_6_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "7":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_7_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "8":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_8_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "9":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_9_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "10":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_10_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "11":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_11_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "12":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_12_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "13":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_13_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "14":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_14_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "15":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_15_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "16":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_16_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "17":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_17_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "18":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_18_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "19":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_19_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "20":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_20_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "21":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_21_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "22":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_22_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "23":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_23_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "24":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_24_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "25":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_25_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "26":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_26_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "27":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_27_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "28":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_28_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "29":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_29_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            case "30":
                if(editTxt_answer_a.getText().toString().equals(ansA) && editTxt_answer_b.getText().toString().equals(ansB) && editTxt_answer_c.getText().toString().equals(ansC)){
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.CORRECT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.SOLUTIONS_30_PASSED, AppConstants.TRUE);
                    }
                    showDialogCorrect(AppConstants.AWESOME);
                }
                else{
                    showDialogIncorrect(AppConstants.TRY_AGAIN);
                }
                break;

            default:
                break;
        }


    }

    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");
                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }

}
