package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class MemoryBoxOWordsActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "MemoryBoxOWordsActivity";
    Intent intent;
    String value, game_type, answer, answer_text = "";
    int score, numQuestions;
    LinearLayout layout;
    View view;
    String[] parts;
    int id = 1;
    TextView trivia, txtView_answer;
    Button btnTag, btn_clear;
    Boolean isClicked1 = false,isClicked2 = false,  isClicked3 = false ,isClicked4 = false,isClicked5 = false,isClicked6 = false,isClicked7 = false,
            isClicked8 = false,isClicked9 = false;

    MediaPlayer mp, mpWrong, mpCorrect;
    TextView timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_box_owords);
        timer = (TextView) findViewById(R.id.timer);
        trivia = (TextView) findViewById(R.id.trivia);
        txtView_answer = (TextView) findViewById(R.id.txtView_answer);
        btn_clear = (Button) findViewById(R.id.btn_clear);
        btn_clear.setOnClickListener(this);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score);
        getIntentData();
        splitString();
        addButtons();

        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }

    public void splitString(){
        parts = answer.split("(?!^)");
        List<String> strList = Arrays.asList(parts);
        Collections.shuffle(strList);
        parts  = strList.toArray(new String[strList.size()]);
    }

    public void addButtons() {
        layout = (LinearLayout) findViewById(R.id.layout);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        for (int j = 0; j < parts.length; j++) {
            btnTag = new Button(this);
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p.weight = 1;
            p.leftMargin = 2;
            p.rightMargin = 2;
            btnTag.setLayoutParams(p);
            btnTag.setBackgroundResource(R.drawable.edittext_blue);
            btnTag.setId(generateUniqueId());
            btnTag.setText(parts[j].toString());
            Log.e("TAG", "addButtons: " + j + " " + parts[j] );
            btnTag.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.e("TAG", "The index is" + v.getId());
                    switch(v.getId()) {
                        case 0:
                            if(!isClicked1)
                            {
                                isClicked1 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;

                        case 1:
                            if(!isClicked2)
                            {
                                isClicked2 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;

                        case 2:
                            if(!isClicked3)
                            {
                                isClicked3 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        case 3:
                            if(!isClicked4)
                            {
                                isClicked4 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        case 4:
                            if(!isClicked5)
                            {
                                isClicked5 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        case 5:
                            if(!isClicked6)
                            {
                                isClicked6 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        case 6:
                            if(!isClicked7)
                            {
                                isClicked7 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        case 7:
                            if(!isClicked8)
                            {
                                isClicked8 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        case 8:
                            if(!isClicked9)
                            {
                                isClicked9 = true;
                                answer_text = answer_text + parts[Integer.valueOf(v.getId()) - 1];
                                txtView_answer.setText(answer_text);
                                if(verifyAnswer(answer_text)){
                                    showDialogCorrect(AppConstants.AWESOME);
                                }
                            }
                            break;
                        default:
                            break;
                    }

                }
            });

            layout.addView(btnTag);
        }
    }

    // Generates and returns a valid id that's not in use
    public int generateUniqueId(){
        View v = findViewById(id);
        while (v != null){
            v = findViewById(++id);
        }
        return id++;
    }


    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.
        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.
        Log.e(TAG, "getIntentData: " + game_type);
        getAnwers();
    }



    public void getAnwers(){
        switch(value){
            case "1":
                answer = AppConstants.BOW_ANSWER_1;
                trivia.setText(AppConstants.BOW_1);
                break;

            case "2":
                answer = AppConstants.BOW_ANSWER_2;
                trivia.setText(AppConstants.BOW_2);
                break;

            case "3":
                answer = AppConstants.BOW_ANSWER_3;
                trivia.setText(AppConstants.BOW_3);
                break;

            case "4":
                answer = AppConstants.BOW_ANSWER_4;
                trivia.setText(AppConstants.BOW_4);
                break;

            case "5":
                answer = AppConstants.BOW_ANSWER_5;
                trivia.setText(AppConstants.BOW_5);
                break;

            case "6":
                answer = AppConstants.BOW_ANSWER_6;
                trivia.setText(AppConstants.BOW_6);
                break;

            case "7":
                answer = AppConstants.BOW_ANSWER_7;
                trivia.setText(AppConstants.BOW_7);
                break;

            case "8":
                answer = AppConstants.BOW_ANSWER_8;
                trivia.setText(AppConstants.BOW_8);
                break;

            case "9":
                answer = AppConstants.BOW_ANSWER_9;
                trivia.setText(AppConstants.BOW_9);
                break;

            case "10":
                answer = AppConstants.BOW_ANSWER_10;
                trivia.setText(AppConstants.BOW_10);
                break;

            case "11":
                answer = AppConstants.BOW_ANSWER_11;
                trivia.setText(AppConstants.BOW_11);
                break;

            case "12":
                answer = AppConstants.BOW_ANSWER_12;
                trivia.setText(AppConstants.BOW_12);
                break;

            case "13":
                answer = AppConstants.BOW_ANSWER_13;
                trivia.setText(AppConstants.BOW_13);
                break;

            case "14":
                answer = AppConstants.BOW_ANSWER_14;
                trivia.setText(AppConstants.BOW_14);
                break;

            case "15":
                answer = AppConstants.BOW_ANSWER_15;
                trivia.setText(AppConstants.BOW_15);
                break;

            case "16":
                answer = AppConstants.BOW_ANSWER_16;
                trivia.setText(AppConstants.BOW_16);
                break;

            case "17":
                answer = AppConstants.BOW_ANSWER_17;
                trivia.setText(AppConstants.BOW_17);
                break;

            case "18":
                answer = AppConstants.BOW_ANSWER_18;
                trivia.setText(AppConstants.BOW_18);
                break;

            case "19":
                answer = AppConstants.BOW_ANSWER_19;
                trivia.setText(AppConstants.BOW_19);
                break;

            case "20":
                answer = AppConstants.BOW_ANSWER_20;
                trivia.setText(AppConstants.BOW_20);
                break;

            case "21":
                answer = AppConstants.BOW_ANSWER_21;
                trivia.setText(AppConstants.BOW_21);
                break;

            case "22":
                answer = AppConstants.BOW_ANSWER_22;
                trivia.setText(AppConstants.BOW_22);

                break;

            case "23":
                answer = AppConstants.BOW_ANSWER_23;
                trivia.setText(AppConstants.BOW_23);

                break;

            case "24":
                answer = AppConstants.BOW_ANSWER_24;
                trivia.setText(AppConstants.BOW_24);

                break;

            case "25":
                answer = AppConstants.BOW_ANSWER_25;
                trivia.setText(AppConstants.BOW_25);

                break;

            case "26":
                answer = AppConstants.BOW_ANSWER_26;
                trivia.setText(AppConstants.BOW_26);

                break;

            case "27":
                answer = AppConstants.BOW_ANSWER_27;
                trivia.setText(AppConstants.BOW_27);

                break;

            case "28":
                answer = AppConstants.BOW_ANSWER_28;
                trivia.setText(AppConstants.BOW_28);

                break;

            case "29":
                answer = AppConstants.BOW_ANSWER_29;
                trivia.setText(AppConstants.BOW_29);

                break;

            case "30":
                answer = AppConstants.BOW_ANSWER_30;
                trivia.setText(AppConstants.BOW_30);

                break;

            default:
                break;
        }
    }

    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.btn_clear:
                btnClickSound();
                answer_text = "";
                txtView_answer.setText(answer_text);
                isClicked1 = false;
                isClicked2 = false;
                isClicked3 = false;
                isClicked4 = false;
                isClicked5 = false;
                isClicked6 = false;
                isClicked7 = false;
                isClicked8 = false;
                isClicked9 = false;
                break;

            default:
                break;
        }
    }

    public boolean verifyAnswer(String answerBuild){
        if(answerBuild.equals(answer))
        {
            Log.e(TAG, "verifyAnswer: "+ game_type);
            switch(value){
                case "1":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_1_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "2":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_2_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "3":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_3_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "4":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_4_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "5":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_5_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "6":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_6_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "7":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_7_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "8":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_8_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "9":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_9_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "10":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_10_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "11":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_11_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "12":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_12_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "13":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_13_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "14":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_14_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "15":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_15_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "16":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_16_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "17":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_17_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "18":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_18_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "19":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_19_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "20":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_20_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "21":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_21_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "22":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_22_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "23":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_23_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "24":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_24_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "25":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_25_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "26":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_26_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "27":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_27_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "28":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_28_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "29":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_29_PASSED, AppConstants.TRUE);
                    }
                    return true;

                case "30":
                    if(game_type.equals(AppConstants.TRAINING)){
                        Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                        Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                    }
                    else{
                        Prefs.putBoolean(AppConstants.BUILDOWORDS_30_PASSED, AppConstants.TRUE);
                    }
                    return true;

                default:

                    return true;
            }
        }
        else{
            return false;
        }
    }

    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }


    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");

                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }
}
