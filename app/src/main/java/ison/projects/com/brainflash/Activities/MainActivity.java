package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import es.dmoral.toasty.Toasty;
import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.Fragments.TabFragment;
import ison.projects.com.brainflash.R;


public class MainActivity extends AppCompatActivity {

    public MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);

        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TabFragment()).commit();


        if(Prefs.getBoolean(AppConstants.IS_FIRST_RUN_MAIN, true)){
            showDialog();
        }

    }

    public void mpStart(){
        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mediaPlayer();
    }

    @Override
    public void onBackPressed() {
        showDialogIncorrect("Do you want to exit?");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mp = MediaPlayer.create(this, R.raw.wrong);
            mp.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mp.stop();
                        finish();
                    }
                })
                .show();
    }

    public void showDialog(){
//        new LovelyTextInputDialog(this, R.style.AppTheme_AppStyled)
//                .setTopColorRes(R.color.infoColor)
//                .setMessage("Name")
//                .setConfirmButton(android.R.string.ok, new LovelyTextInputDialog.OnTextInputConfirmListener() {
//                    @Override
//                    public void onTextInputConfirmed(String text) {
//                        Prefs.putString(AppConstants.NAME, text);
//                        Toasty.success(getApplicationContext(), "SAVED", Toast.LENGTH_SHORT, true).show();
//                        Prefs.putBoolean(AppConstants.IS_FIRST_RUN_MAIN, false);
//                        Intent refresh = new Intent(getApplication(), MainActivity.class);
//                        startActivity(refresh);//Start the same Activity
//                        finish();
//                    }
//                })
//                .show();
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.prompt,null);

        // Specify alert dialog is not cancelable/not ignorable
        builder.setCancelable(false);

        // Set the custom layout as alert dialog view
        builder.setView(dialogView);

        // Get the custom alert dialog view widgets reference
        Button btn_positive = (Button) dialogView.findViewById(R.id.dialog_positive_btn);
        final EditText et_name = (EditText) dialogView.findViewById(R.id.et_name);

        // Create the alert dialog
        final AlertDialog dialog = builder.create();

        // Set positive/yes button click listener
        btn_positive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dismiss the alert dialog
                dialog.cancel();
                String name = et_name.getText().toString();
                        Prefs.putString(AppConstants.NAME, name);
                        Toasty.success(getApplicationContext(), "SAVED", Toast.LENGTH_SHORT, true).show();
                        Prefs.putBoolean(AppConstants.IS_FIRST_RUN_MAIN, false);
                        Intent refresh = new Intent(getApplication(), MainActivity.class);
                        startActivity(refresh);//Start the same Activity

            }
        });

        // Display the custom alert dialog on interface
        dialog.show();


    }



}