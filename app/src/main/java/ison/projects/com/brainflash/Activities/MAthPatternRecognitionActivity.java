package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pixplicity.easyprefs.library.Prefs;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import java.util.ArrayList;
import java.util.Collections;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class MAthPatternRecognitionActivity extends AppCompatActivity {

    private static final String TAG = "MAthPatternRecognitionA";
    Intent intent;
    String value, game_type;
    int score, numQuestions;
    int id = 1;
    LinearLayout layout;
    ImageView question;
    MediaPlayer mp, mpWrong, mpCorrect;
    TextView timer;

    ArrayList<Button> buttonList = new ArrayList<Button>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_recognition);

        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        getIntentData();

    }

    public void initUI(){
        timer = (TextView) findViewById(R.id.timer);
        question = (ImageView) findViewById(R.id.question);
        layout = (LinearLayout) findViewById(R.id.layout);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        score = Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0);
        numQuestions = Prefs.getInt(AppConstants.NUMBER_OF_QUESTIONS, 0);
        Log.e(TAG, "initUI: " + score);
        addButtons();

        if(game_type.equals( AppConstants.TRAINING)){
            countdown();
        }
    }

    // Generates and returns a valid id that's not in use
    public int generateUniqueId(){
        View v = findViewById(id);
        while (v != null){
            v = findViewById(++id);
        }
        return id++;
    }


    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }
    public void dealWithButtonClick(Button b) {
        switch(b.getId()) {
            case 1:
                btnClickSound();
                showDialogCorrect(AppConstants.AWESOME);
                break;

            case 2:
                btnClickSound();
                showDialogIncorrect(AppConstants.TRY_AGAIN);
                break;

            case 3:
                btnClickSound();
                showDialogIncorrect(AppConstants.TRY_AGAIN);
                break;

            case 4:
                btnClickSound();
                showDialogIncorrect(AppConstants.TRY_AGAIN);
                break;
            default:
                break;
        }
    }


    public void addButtons(){

        ArrayList<Button> buttonList = new ArrayList<Button>();

        for (int i = 0; i < 4; i++) {
            final Button b = new Button(this);
            b.setGravity(Gravity.CENTER_HORIZONTAL);
            b.setId(generateUniqueId());                    // Set an id to Button
            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dealWithButtonClick(b);
                }
            });

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(10, 10, 10, 10);
            params.weight = 1;
            params.height = 250;
            b.setLayoutParams(params);
            b.setGravity(Gravity.CENTER);

            switch(b.getId()){
                case 1:
                    switch (value){
                        case "1":
                            question.setBackgroundResource(R.drawable.lvl_1);
                            b.setBackgroundResource(R.drawable.lvl_1_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_1_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "2":
                            question.setBackgroundResource(R.drawable.lvl_2);
                            b.setBackgroundResource(R.drawable.lvl_2_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_2_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "3":
                            question.setBackgroundResource(R.drawable.lvl_3);
                            b.setBackgroundResource(R.drawable.lvl_3_a);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_3_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "4":
                            question.setBackgroundResource(R.drawable.lvl_4);
                            b.setBackgroundResource(R.drawable.lvl_4_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_4_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "5":
                            question.setBackgroundResource(R.drawable.lvl_5);
                            b.setBackgroundResource(R.drawable.lvl_5_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_5_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "6":
                            question.setBackgroundResource(R.drawable.lvl_6);
                            b.setBackgroundResource(R.drawable.lvl_6_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_6_PASSED, AppConstants.TRUE);
                            }
                            break;

                        case "7":
                            question.setBackgroundResource(R.drawable.lvl_7);
                            b.setBackgroundResource(R.drawable.lvl_7_a);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_7_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "8":
                            question.setBackgroundResource(R.drawable.lvl_8);
                            b.setBackgroundResource(R.drawable.lvl_8_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_8_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "9":
                            question.setBackgroundResource(R.drawable.lvl_9);
                            b.setBackgroundResource(R.drawable.lvl_9_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_9_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "10":
                            question.setBackgroundResource(R.drawable.lvl_10);
                            b.setBackgroundResource(R.drawable.lvl_10_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_10_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "11":
                            question.setBackgroundResource(R.drawable.lvl_11);
                            b.setBackgroundResource(R.drawable.lvl_11_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_11_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "12":
                            question.setBackgroundResource(R.drawable.lvl_12);
                            b.setBackgroundResource(R.drawable.lvl_12_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_12_PASSED, AppConstants.TRUE);
                            }
                            break;

                        case "13":
                            question.setBackgroundResource(R.drawable.lvl_13);
                            b.setBackgroundResource(R.drawable.lvl_13_a);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_13_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "14":
                            question.setBackgroundResource(R.drawable.lvl_14);
                            b.setBackgroundResource(R.drawable.lvl_14_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_14_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "15":
                            question.setBackgroundResource(R.drawable.lvl_15);
                            b.setBackgroundResource(R.drawable.lvl_15_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_15_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "16":
                            question.setBackgroundResource(R.drawable.lvl_16);
                            b.setBackgroundResource(R.drawable.lvl_16_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_16_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "17":
                            question.setBackgroundResource(R.drawable.lvl_17);
                            b.setBackgroundResource(R.drawable.lvl_17_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_17_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "18":
                            question.setBackgroundResource(R.drawable.lvl_18);
                            b.setBackgroundResource(R.drawable.lvl_18_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_18_PASSED, AppConstants.TRUE);
                            }
                            break;

                        case "19":
                            question.setBackgroundResource(R.drawable.lvl_19);
                            b.setBackgroundResource(R.drawable.lvl_19_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_19_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "20":
                            question.setBackgroundResource(R.drawable.lvl_20);
                            b.setBackgroundResource(R.drawable.lvl_20_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_20_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "21":
                            question.setBackgroundResource(R.drawable.lvl_21);
                            b.setBackgroundResource(R.drawable.lvl_21_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_21_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "22":
                            question.setBackgroundResource(R.drawable.lvl_22);
                            b.setBackgroundResource(R.drawable.lvl_22_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_22_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "23":
                            question.setBackgroundResource(R.drawable.lvl_23);
                            b.setBackgroundResource(R.drawable.lvl_23_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_23_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "24":
                            question.setBackgroundResource(R.drawable.lvl_24);
                            b.setBackgroundResource(R.drawable.lvl_24_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_24_PASSED, AppConstants.TRUE);
                            }
                            break;

                        case "25":
                            question.setBackgroundResource(R.drawable.lvl_25);
                            b.setBackgroundResource(R.drawable.lvl_25_d);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_25_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "26":
                            question.setBackgroundResource(R.drawable.lvl_26);
                            b.setBackgroundResource(R.drawable.lvl_26_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_26_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "27":
                            question.setBackgroundResource(R.drawable.lvl_27);
                            b.setBackgroundResource(R.drawable.lvl_27_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_27_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "28":
                            question.setBackgroundResource(R.drawable.lvl_28);
                            b.setBackgroundResource(R.drawable.lvl_28_a);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_28_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "29":
                            question.setBackgroundResource(R.drawable.lvl_29);
                            b.setBackgroundResource(R.drawable.lvl_29_c);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_29_PASSED, AppConstants.TRUE);
                            }
                            break;
                        case "30":
                            question.setBackgroundResource(R.drawable.lvl_30);
                            b.setBackgroundResource(R.drawable.lvl_30_b);
                            if(game_type.equals(AppConstants.TRAINING)){
                                Prefs.putInt(AppConstants.TRAINING_ANSWER_COUNT, score + 1);
                                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                            }
                            else{
                                Prefs.putBoolean(AppConstants.PATTERN_RECOGINITION_30_PASSED, AppConstants.TRUE);
                            }
                            break;

                        default:
                            break;
                    }
                    break;
                case 2:
                    switch (value){
                        case "1":
                            b.setBackgroundResource(R.drawable.lvl_1_a);
                            break;
                        case "2":
                            b.setBackgroundResource(R.drawable.lvl_2_a);
                            break;
                        case "3":
                            b.setBackgroundResource(R.drawable.lvl_3_b);
                            break;
                        case "4":
                            b.setBackgroundResource(R.drawable.lvl_4_a);
                            break;
                        case "5":
                            b.setBackgroundResource(R.drawable.lvl_5_a);
                            break;
                        case "6":
                            b.setBackgroundResource(R.drawable.lvl_6_a);
                            break;

                        case "7":
                            b.setBackgroundResource(R.drawable.lvl_7_b);
                            break;
                        case "8":
                            b.setBackgroundResource(R.drawable.lvl_8_a);
                            break;
                        case "9":
                            b.setBackgroundResource(R.drawable.lvl_9_a);
                            break;
                        case "10":
                            b.setBackgroundResource(R.drawable.lvl_10_a);
                            break;
                        case "11":
                            b.setBackgroundResource(R.drawable.lvl_11_a);
                            break;
                        case "12":
                            b.setBackgroundResource(R.drawable.lvl_12_a);
                            break;

                        case "13":
                            b.setBackgroundResource(R.drawable.lvl_13_b);
                            break;
                        case "14":
                            b.setBackgroundResource(R.drawable.lvl_14_a);
                            break;
                        case "15":
                            b.setBackgroundResource(R.drawable.lvl_15_a);
                            break;
                        case "16":
                            b.setBackgroundResource(R.drawable.lvl_16_a);
                            break;
                        case "17":
                            b.setBackgroundResource(R.drawable.lvl_17_a);
                            break;
                        case "18":
                            b.setBackgroundResource(R.drawable.lvl_18_a);
                            break;

                        case "19":
                            b.setBackgroundResource(R.drawable.lvl_19_a);
                            break;
                        case "20":
                            b.setBackgroundResource(R.drawable.lvl_20_a);
                            break;
                        case "21":
                            b.setBackgroundResource(R.drawable.lvl_21_a);
                            break;
                        case "22":
                            b.setBackgroundResource(R.drawable.lvl_22_a);
                            break;
                        case "23":
                            b.setBackgroundResource(R.drawable.lvl_23_a);
                            break;
                        case "24":
                            b.setBackgroundResource(R.drawable.lvl_24_a);
                            break;

                        case "25":
                            b.setBackgroundResource(R.drawable.lvl_25_a);
                            break;
                        case "26":
                            b.setBackgroundResource(R.drawable.lvl_26_a);
                            break;
                        case "27":
                            b.setBackgroundResource(R.drawable.lvl_27_a);
                            break;
                        case "28":
                            b.setBackgroundResource(R.drawable.lvl_28_b);
                            break;
                        case "29":
                            b.setBackgroundResource(R.drawable.lvl_29_a);
                            break;
                        case "30":
                            b.setBackgroundResource(R.drawable.lvl_30_a);
                            break;

                        default:
                            break;
                    }
                    break;

                case 3:
                    switch (value){
                        case "1":
                            b.setBackgroundResource(R.drawable.lvl_1_c);
                            break;
                        case "2":
                            b.setBackgroundResource(R.drawable.lvl_2_b);
                            break;
                        case "3":
                            b.setBackgroundResource(R.drawable.lvl_3_c);
                            break;
                        case "4":
                            b.setBackgroundResource(R.drawable.lvl_4_b);
                            break;
                        case "5":
                            b.setBackgroundResource(R.drawable.lvl_5_b);
                            break;
                        case "6":
                            b.setBackgroundResource(R.drawable.lvl_6_c);
                            break;

                        case "7":
                            b.setBackgroundResource(R.drawable.lvl_7_c);
                            break;
                        case "8":
                            b.setBackgroundResource(R.drawable.lvl_8_b);
                            break;
                        case "9":
                            b.setBackgroundResource(R.drawable.lvl_9_c);
                            break;
                        case "10":
                            b.setBackgroundResource(R.drawable.lvl_10_b);
                            break;
                        case "11":
                            b.setBackgroundResource(R.drawable.lvl_11_c);
                            break;
                        case "12":
                            b.setBackgroundResource(R.drawable.lvl_12_b);
                            break;

                        case "13":
                            b.setBackgroundResource(R.drawable.lvl_13_c);
                            break;
                        case "14":
                            b.setBackgroundResource(R.drawable.lvl_14_c);
                            break;
                        case "15":
                            b.setBackgroundResource(R.drawable.lvl_15_b);
                            break;
                        case "16":
                            b.setBackgroundResource(R.drawable.lvl_16_c);
                            break;
                        case "17":
                            b.setBackgroundResource(R.drawable.lvl_17_b);
                            break;
                        case "18":
                            b.setBackgroundResource(R.drawable.lvl_18_b);
                            break;

                        case "19":
                            b.setBackgroundResource(R.drawable.lvl_19_b);
                            break;
                        case "20":
                            b.setBackgroundResource(R.drawable.lvl_20_b);
                            break;
                        case "21":
                            b.setBackgroundResource(R.drawable.lvl_21_b);
                            break;
                        case "22":
                            b.setBackgroundResource(R.drawable.lvl_22_b);
                            break;
                        case "23":
                            b.setBackgroundResource(R.drawable.lvl_23_b);
                            break;
                        case "24":
                            b.setBackgroundResource(R.drawable.lvl_24_b);
                            break;

                        case "25":
                            b.setBackgroundResource(R.drawable.lvl_25_b);
                            break;
                        case "26":
                            b.setBackgroundResource(R.drawable.lvl_26_b);
                            break;
                        case "27":
                            b.setBackgroundResource(R.drawable.lvl_27_c);
                            break;
                        case "28":
                            b.setBackgroundResource(R.drawable.lvl_28_c);
                            break;
                        case "29":
                            b.setBackgroundResource(R.drawable.lvl_29_b);
                            break;
                        case "30":
                            b.setBackgroundResource(R.drawable.lvl_30_b);
                            break;

                        default:
                            break;
                    }
                    break;

                case 4:
                    switch (value){
                        case "1":
                            b.setBackgroundResource(R.drawable.lvl_1_d);
                            break;
                        case "2":
                            b.setBackgroundResource(R.drawable.lvl_2_c);
                            break;
                        case "3":
                            b.setBackgroundResource(R.drawable.lvl_3_d);
                            break;
                        case "4":
                            b.setBackgroundResource(R.drawable.lvl_4_c);
                            break;
                        case "5":
                            b.setBackgroundResource(R.drawable.lvl_5_d);
                            break;
                        case "6":
                            b.setBackgroundResource(R.drawable.lvl_6_d);
                            break;

                        case "7":
                            b.setBackgroundResource(R.drawable.lvl_7_d);
                            break;
                        case "8":
                            b.setBackgroundResource(R.drawable.lvl_8_d);
                            break;
                        case "9":
                            b.setBackgroundResource(R.drawable.lvl_9_d);
                            break;
                        case "10":
                            b.setBackgroundResource(R.drawable.lvl_10_d);
                            break;
                        case "11":
                            b.setBackgroundResource(R.drawable.lvl_11_d);
                            break;
                        case "12":
                            b.setBackgroundResource(R.drawable.lvl_12_c);
                            break;

                        case "13":
                            b.setBackgroundResource(R.drawable.lvl_13_d);
                            break;
                        case "14":
                            b.setBackgroundResource(R.drawable.lvl_14_d);
                            break;
                        case "15":
                            b.setBackgroundResource(R.drawable.lvl_15_d);
                            break;
                        case "16":
                            b.setBackgroundResource(R.drawable.lvl_16_d);
                            break;
                        case "17":
                            b.setBackgroundResource(R.drawable.lvl_17_d);
                            break;
                        case "18":
                            b.setBackgroundResource(R.drawable.lvl_18_d);
                            break;

                        case "19":
                            b.setBackgroundResource(R.drawable.lvl_19_d);
                            break;
                        case "20":
                            b.setBackgroundResource(R.drawable.lvl_20_c);
                            break;
                        case "21":
                            b.setBackgroundResource(R.drawable.lvl_21_c);
                            break;
                        case "22":
                            b.setBackgroundResource(R.drawable.lvl_22_c);
                            break;
                        case "23":
                            b.setBackgroundResource(R.drawable.lvl_23_d);
                            break;
                        case "24":
                            b.setBackgroundResource(R.drawable.lvl_24_d);
                            break;

                        case "25":
                            b.setBackgroundResource(R.drawable.lvl_25_c);
                            break;
                        case "26":
                            b.setBackgroundResource(R.drawable.lvl_26_d);
                            break;
                        case "27":
                            b.setBackgroundResource(R.drawable.lvl_27_d);
                            break;
                        case "28":
                            b.setBackgroundResource(R.drawable.lvl_28_d);
                            break;
                        case "29":
                            b.setBackgroundResource(R.drawable.lvl_29_d);
                            break;
                        case "30":
                            b.setBackgroundResource(R.drawable.lvl_30_c);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
            buttonList.add(b);
        }

// Shuffle
        Collections.shuffle(buttonList);
        for (int i = 0; i < 4; i++) {
            if (i < 4) {
                layout.addView(buttonList.get(i));
            } else {
                //Do Nothing
            }
        }

    }
    public void showDialogCorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpCorrect = MediaPlayer.create(this, R.raw.correct);
            mpCorrect.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.green)
                .setButtonsColorRes(R.color.green)
                .setIcon(R.drawable.correct)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        finish();
//                        Toast.makeText(getApplication(), "positive clicked", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();
    }

    public void showDialogIncorrect(String result) {
        if( Prefs.getBoolean(AppConstants.IS_SOUND_ON, AppConstants.TRUE)){
            mpWrong = MediaPlayer.create(this, R.raw.wrong);
            mpWrong.start();
        }
        new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.VERTICAL)
                .setTopColorRes(R.color.red)
                .setButtonsColorRes(R.color.red)
                .setIcon(R.drawable.incorrect)
                .setTitle(result)
                .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                })
                .show();
    }

    public void getIntentData(){
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.LEVEL); //if it's a string you stored.
        game_type = intent.getStringExtra(AppConstants.GAME_TYPE); //if it's a string you stored.

    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    @Override
    public void onBackPressed() {
        if(game_type.equals( AppConstants.TRAINING)){
//            sendIntent();
            Prefs.putBoolean(AppConstants.IS_ACTIVITY, true);
            Prefs.putBoolean(AppConstants.IS_BACK_PRESSED, true);
            Log.e(TAG, "onBackPressed: " + Prefs.getBoolean(AppConstants.IS_BACK_PRESSED, true) );
            finish();
        }
        else{
            super.onBackPressed();
        }
    }

    public void sendIntent(){
        intent = new Intent(this, TrainingActvity.class);
        intent.putExtra(AppConstants.ISEXIT, AppConstants.ISEXIT);
        intent.putExtra(AppConstants.IS_ACTIVITY, AppConstants.IS_NOT_ACTIVITY);
        this.startActivity(intent);
    }

    public void countdown(){
        timer.setVisibility(View.VISIBLE);
        new CountDownTimer(10000, 1000) {

            public void onTick(long millisUntilFinished) {
//                Toasty.warning(getApplication(), String.valueOf( millisUntilFinished / 1000), Toast.LENGTH_SHORT, true).show();
                timer.setText("seconds remaining: " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
//                mTextField.setText("done!");

                Prefs.putInt(AppConstants.NUMBER_OF_QUESTIONS, numQuestions + 1);
                finish();
            }

        }.start();
    }
}
