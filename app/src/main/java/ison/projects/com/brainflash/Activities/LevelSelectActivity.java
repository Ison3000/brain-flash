package ison.projects.com.brainflash.Activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.pixplicity.easyprefs.library.Prefs;

import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

public class LevelSelectActivity extends AppCompatActivity implements View.OnClickListener{

    Intent intent;
    String value;
    private static final String TAG = "LevelSelectActivity";
    public MediaPlayer mp;

    View view;
    TextView actvity_name;

    Button lvl1,lvl2, lvl3, lvl4, lvl5, lvl6, lvl7, lvl8, lvl9, lvl10, lvl11, lvl12, lvl13, lvl14, lvl15, lvl16, lvl17,
            lvl18, lvl19, lvl20, lvl21, lvl22, lvl23, lvl24, lvl25, lvl26, lvl27, lvl28, lvl29, lvl30;

    Class activity_dest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_level_select);

        getIntentData();
        Log.d(TAG, "onCreate: " + value);
    }

    public void initUI(){
        lvl1 = (Button) findViewById(R.id.lvl1);
        lvl1.setOnClickListener(this);
        lvl2 = (Button) findViewById(R.id.lvl2);
        lvl2.setOnClickListener(this);
        lvl3 = (Button) findViewById(R.id.lvl3);
        lvl3.setOnClickListener(this);
        lvl4 = (Button) findViewById(R.id.lvl4);
        lvl4.setOnClickListener(this);
        lvl5 = (Button) findViewById(R.id.lvl5);
        lvl5.setOnClickListener(this);
        lvl6 = (Button) findViewById(R.id.lvl6);
        lvl6.setOnClickListener(this);
        lvl7 = (Button) findViewById(R.id.lvl7);
        lvl7.setOnClickListener(this);
        lvl8 = (Button) findViewById(R.id.lvl8);
        lvl8.setOnClickListener(this);
        lvl9 = (Button) findViewById(R.id.lvl9);
        lvl9.setOnClickListener(this);
        lvl10 = (Button) findViewById(R.id.lvl10);
        lvl10.setOnClickListener(this);
        lvl11 = (Button) findViewById(R.id.lvl11);
        lvl11.setOnClickListener(this);
        lvl12 = (Button) findViewById(R.id.lvl12);
        lvl12.setOnClickListener(this);
        lvl13 = (Button) findViewById(R.id.lvl13);
        lvl13.setOnClickListener(this);
        lvl14 = (Button) findViewById(R.id.lvl14);
        lvl14.setOnClickListener(this);
        lvl15 = (Button) findViewById(R.id.lvl15);
        lvl15.setOnClickListener(this);
        lvl16 = (Button) findViewById(R.id.lvl16);
        lvl16.setOnClickListener(this);
        lvl17 = (Button) findViewById(R.id.lvl17);
        lvl17.setOnClickListener(this);
        lvl18 = (Button) findViewById(R.id.lvl18);
        lvl18.setOnClickListener(this);
        lvl19 = (Button) findViewById(R.id.lvl19);
        lvl19.setOnClickListener(this);
        lvl20 = (Button) findViewById(R.id.lvl20);
        lvl20.setOnClickListener(this);
        lvl21 = (Button) findViewById(R.id.lvl21);
        lvl21.setOnClickListener(this);
        lvl22 = (Button) findViewById(R.id.lvl22);
        lvl22.setOnClickListener(this);
        lvl23 = (Button) findViewById(R.id.lvl23);
        lvl23.setOnClickListener(this);
        lvl24 = (Button) findViewById(R.id.lvl24);
        lvl24.setOnClickListener(this);
        lvl25 = (Button) findViewById(R.id.lvl25);
        lvl25.setOnClickListener(this);
        lvl26 = (Button) findViewById(R.id.lvl26);
        lvl26.setOnClickListener(this);
        lvl27 = (Button) findViewById(R.id.lvl27);
        lvl27.setOnClickListener(this);
        lvl28 = (Button) findViewById(R.id.lvl28);
        lvl28.setOnClickListener(this);
        lvl29 = (Button) findViewById(R.id.lvl29);
        lvl29.setOnClickListener(this);
        lvl30 = (Button) findViewById(R.id.lvl30);
        lvl30.setOnClickListener(this);
        mp = MediaPlayer.create(this, R.raw.bg_music);
        mp.setLooping(true);
        checkLevels();
    }



    public void checkLevels(){
        Log.e(TAG, "checkLevels: " );
        view = getWindow().getDecorView().getRootView();
        switch (value){
            case AppConstants.ACTIVITY_MATH_NUMBER_TRICKS:

                if(Prefs.getBoolean(AppConstants.NUMTRICKS_1_PASSED, AppConstants.FALSE)){
                    Log.e(TAG, "checkLevels: " );
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                    lvl2.setText("2");
                }

                if(Prefs.getBoolean(AppConstants.NUMTRICKS_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                
                    lvl2.setText("3");
                    
                }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_3_PASSED, AppConstants.FALSE)){
                        lvl4.setBackgroundResource(R.drawable.edittext_black);
                        lvl4.setEnabled(true);
                
                    lvl2.setText("4");
                    }
                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_4_PASSED, AppConstants.FALSE)){
                        lvl5.setBackgroundResource(R.drawable.edittext_black);
                        lvl5.setEnabled(true);
                    
                    lvl2.setText("5");
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_5_PASSED, AppConstants.FALSE)){
                        lvl6.setBackgroundResource(R.drawable.edittext_black);
                        lvl6.setEnabled(true);
                    
                    lvl2.setText("6");
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_6_PASSED, AppConstants.FALSE)){
                        lvl7.setBackgroundResource(R.drawable.edittext_black);
                        lvl7.setEnabled(true);
                    
                    lvl2.setText("7");
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_7_PASSED, AppConstants.FALSE)){
                        lvl8.setBackgroundResource(R.drawable.edittext_black);
                        lvl8.setEnabled(true);
                    
                    lvl2.setText("8");
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_8_PASSED, AppConstants.FALSE)){
                        lvl9.setBackgroundResource(R.drawable.edittext_black);
                        lvl9.setEnabled(true);
                    lvl2.setText("9");
                    
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_9_PASSED, AppConstants.FALSE)){
                        lvl10.setBackgroundResource(R.drawable.edittext_black);
                        lvl10.setEnabled(true);
                    
                    lvl2.setText("10");
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_10_PASSED, AppConstants.FALSE)){
                        lvl11.setBackgroundResource(R.drawable.edittext_black);
                        lvl11.setEnabled(true);
                                        lvl2.setText("11");
                        
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_11_PASSED, AppConstants.FALSE)){
                        lvl12.setBackgroundResource(R.drawable.edittext_black);
                        lvl12.setEnabled(true);
                                            lvl2.setText("12");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_12_PASSED, AppConstants.FALSE)){
                        lvl13.setBackgroundResource(R.drawable.edittext_black);
                        lvl13.setEnabled(true);
                                            lvl2.setText("13");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_13_PASSED, AppConstants.FALSE)){
                        lvl14.setBackgroundResource(R.drawable.edittext_black);
                        lvl14.setEnabled(true);
                                            lvl2.setText("14");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_14_PASSED, AppConstants.FALSE)){
                        lvl15.setBackgroundResource(R.drawable.edittext_black);
                        lvl15.setEnabled(true);
                                            lvl2.setText("15");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_15_PASSED, AppConstants.FALSE)){
                        lvl16.setBackgroundResource(R.drawable.edittext_black);
                        lvl16.setEnabled(true);
                                            lvl2.setText("16");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_16_PASSED, AppConstants.FALSE)){
                        lvl17.setBackgroundResource(R.drawable.edittext_black);
                        lvl17.setEnabled(true);
                                            lvl2.setText("17");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_17_PASSED, AppConstants.FALSE)){
                        lvl18.setBackgroundResource(R.drawable.edittext_black);
                        lvl18.setEnabled(true);
                                            lvl2.setText("18");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_18_PASSED, AppConstants.FALSE)){
                        lvl19.setBackgroundResource(R.drawable.edittext_black);
                        lvl19.setEnabled(true);
                                            lvl2.setText("19");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_19_PASSED, AppConstants.FALSE)){
                        lvl20.setBackgroundResource(R.drawable.edittext_black);
                        lvl20.setEnabled(true);
                                            lvl2.setText("20");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_20_PASSED, AppConstants.FALSE)){
                        lvl21.setBackgroundResource(R.drawable.edittext_black);
                        lvl21.setEnabled(true);
                                                                    lvl2.setText("21");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_21_PASSED, AppConstants.FALSE)){
                        lvl22.setBackgroundResource(R.drawable.edittext_black);
                        lvl22.setEnabled(true);
                                            lvl2.setText("22");
                                            }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_22_PASSED, AppConstants.FALSE)){
                        lvl23.setBackgroundResource(R.drawable.edittext_black);
                        lvl23.setEnabled(true);
                                                                    lvl2.setText("23");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_23_PASSED, AppConstants.FALSE)){
                        lvl24.setBackgroundResource(R.drawable.edittext_black);
                        lvl24.setEnabled(true);
                                                                    lvl2.setText("24");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_24_PASSED, AppConstants.FALSE)){
                        lvl25.setBackgroundResource(R.drawable.edittext_black);
                        lvl25.setEnabled(true);
                                                                    lvl2.setText("25");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_25_PASSED, AppConstants.FALSE)){
                        lvl26.setBackgroundResource(R.drawable.edittext_black);
                        lvl26.setEnabled(true);
                                                                    lvl2.setText("26");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_26_PASSED, AppConstants.FALSE)){
                        lvl27.setBackgroundResource(R.drawable.edittext_black);
                        lvl27.setEnabled(true);
                                                                    lvl2.setText("27");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_27_PASSED, AppConstants.FALSE)){
                        lvl28.setBackgroundResource(R.drawable.edittext_black);
                        lvl28.setEnabled(true);
                                                                    lvl2.setText("28");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_28_PASSED, AppConstants.FALSE)){
                        lvl29.setBackgroundResource(R.drawable.edittext_black);
                        lvl29.setEnabled(true);
                                                                    lvl2.setText("29");
                    }

                    if(Prefs.getBoolean(AppConstants.NUMTRICKS_29_PASSED, AppConstants.FALSE)){
                        lvl30.setBackgroundResource(R.drawable.edittext_black);
                        lvl30.setEnabled(true);
                                                                    lvl2.setText("30");
                    }
                
                break;

            case AppConstants.ACTIVITY_MATH_OPERATIONS:

                if(Prefs.getBoolean(AppConstants.OPERATION_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.OPERATION_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }
                if(Prefs.getBoolean(AppConstants.OPERATION_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }
                if(Prefs.getBoolean(AppConstants.OPERATION_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.OPERATION_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }

                break;


            case AppConstants.ACTIVITY_MATH_PATTERN_RECOGNITON:

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }
                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }
                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.PATTERN_RECOGINITION_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }

                break;

            case AppConstants.ACTIVITY_LOGIC_RIDDLES:
                if(Prefs.getBoolean(AppConstants.RIDDLES_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.RIDDLES_29_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.RIDDLES_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }

                break;

            case AppConstants.ACTIVITY_LOGIC_SOLUTIONS:

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }



                if(Prefs.getBoolean(AppConstants.SOLUTIONS_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SOLUTIONS_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SOLUTIONS_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }
                break;

            case AppConstants.ACTIVITY_LOGIC_WHO_I_AM:
                if(Prefs.getBoolean(AppConstants.WHOIAM_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }



                if(Prefs.getBoolean(AppConstants.WHOIAM_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }



                if(Prefs.getBoolean(AppConstants.WHOIAM_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WHOIAM_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WHOIAM_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WHOIAM_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WHOIAM_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WHOIAM_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }
                break;

            case AppConstants.ACTIVITY_MEMORY_WORD_QUIZ:

                if(Prefs.getBoolean(AppConstants.WORDQUIZ_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WORDQUIZ_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WORDQUIZ_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WORDQUIZ_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.WORDQUIZ_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.WORDQUIZ_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }

                break;

            case AppConstants.ACTIVITY_MEMORY_BUILD_O_WORDS:

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }



                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_25_PASSED, AppConstants.FALSE)) {
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }



                if(Prefs.getBoolean(AppConstants.BUILDOWORDS_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }

                break;

            case AppConstants.ACTIVITY_MEMORY_SYNTAX:

                if(Prefs.getBoolean(AppConstants.SYNTAX_1_PASSED, AppConstants.FALSE)){
                    lvl2.setBackgroundResource(R.drawable.edittext_black);
                    lvl2.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_2_PASSED, AppConstants.FALSE)){
                    lvl3.setBackgroundResource(R.drawable.edittext_black);
                    lvl3.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_3_PASSED, AppConstants.FALSE)){
                    lvl4.setBackgroundResource(R.drawable.edittext_black);
                    lvl4.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_4_PASSED, AppConstants.FALSE)){
                    lvl5.setBackgroundResource(R.drawable.edittext_black);
                    lvl5.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_5_PASSED, AppConstants.FALSE)){
                    lvl6.setBackgroundResource(R.drawable.edittext_black);
                    lvl6.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_6_PASSED, AppConstants.FALSE)){
                    lvl7.setBackgroundResource(R.drawable.edittext_black);
                    lvl7.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_7_PASSED, AppConstants.FALSE)){
                    lvl8.setBackgroundResource(R.drawable.edittext_black);
                    lvl8.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_8_PASSED, AppConstants.FALSE)){
                    lvl9.setBackgroundResource(R.drawable.edittext_black);
                    lvl9.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_9_PASSED, AppConstants.FALSE)){
                    lvl10.setBackgroundResource(R.drawable.edittext_black);
                    lvl10.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_10_PASSED, AppConstants.FALSE)){
                    lvl11.setBackgroundResource(R.drawable.edittext_black);
                    lvl11.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_11_PASSED, AppConstants.FALSE)){
                    lvl12.setBackgroundResource(R.drawable.edittext_black);
                    lvl12.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_12_PASSED, AppConstants.FALSE)){
                    lvl13.setBackgroundResource(R.drawable.edittext_black);
                    lvl13.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_13_PASSED, AppConstants.FALSE)){
                    lvl14.setBackgroundResource(R.drawable.edittext_black);
                    lvl14.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_14_PASSED, AppConstants.FALSE)){
                    lvl15.setBackgroundResource(R.drawable.edittext_black);
                    lvl15.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_15_PASSED, AppConstants.FALSE)){
                    lvl16.setBackgroundResource(R.drawable.edittext_black);
                    lvl16.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_16_PASSED, AppConstants.FALSE)){
                    lvl17.setBackgroundResource(R.drawable.edittext_black);
                    lvl17.setEnabled(true);
                }

                if(Prefs.getBoolean(AppConstants.SYNTAX_17_PASSED, AppConstants.FALSE)){
                    lvl18.setBackgroundResource(R.drawable.edittext_black);
                    lvl18.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_18_PASSED, AppConstants.FALSE)){
                    lvl19.setBackgroundResource(R.drawable.edittext_black);
                    lvl19.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_19_PASSED, AppConstants.FALSE)){
                    lvl20.setBackgroundResource(R.drawable.edittext_black);
                    lvl20.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_20_PASSED, AppConstants.FALSE)){
                    lvl21.setBackgroundResource(R.drawable.edittext_black);
                    lvl21.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_21_PASSED, AppConstants.FALSE)){
                    lvl22.setBackgroundResource(R.drawable.edittext_black);
                    lvl22.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_22_PASSED, AppConstants.FALSE)){
                    lvl23.setBackgroundResource(R.drawable.edittext_black);
                    lvl23.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_23_PASSED, AppConstants.FALSE)){
                    lvl24.setBackgroundResource(R.drawable.edittext_black);
                    lvl24.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_24_PASSED, AppConstants.FALSE)){
                    lvl25.setBackgroundResource(R.drawable.edittext_black);
                    lvl25.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_25_PASSED, AppConstants.FALSE)){
                    lvl26.setBackgroundResource(R.drawable.edittext_black);
                    lvl26.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_26_PASSED, AppConstants.FALSE)){
                    lvl27.setBackgroundResource(R.drawable.edittext_black);
                    lvl27.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_27_PASSED, AppConstants.FALSE)){
                    lvl28.setBackgroundResource(R.drawable.edittext_black);
                    lvl28.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_28_PASSED, AppConstants.FALSE)){
                    lvl29.setBackgroundResource(R.drawable.edittext_black);
                    lvl29.setEnabled(true);
                }


                if(Prefs.getBoolean(AppConstants.SYNTAX_29_PASSED, AppConstants.FALSE)){
                    lvl30.setBackgroundResource(R.drawable.edittext_black);
                    lvl30.setEnabled(true);
                }
                break;

            default:
                break;
        }
    }


    public void getIntentData(){
        actvity_name = (TextView) findViewById(R.id.activity_name);
        intent = getIntent();
        value = intent.getStringExtra(AppConstants.ACTIVITY); //if it's a string you stored.

        switch (value) {
            case AppConstants.ACTIVITY_MATH_NUMBER_TRICKS:
                actvity_name.setText("NUMTRICKS");
                activity_dest = MathNumberTricksActivity.class;
                break;

            case AppConstants.ACTIVITY_MATH_OPERATIONS:
                actvity_name.setText("OPERATIONS");
                activity_dest = MAthOperationActivity.class;
                break;

            case AppConstants.ACTIVITY_MATH_PATTERN_RECOGNITON:
                actvity_name.setText("PATTERN RECOGNITION");
                activity_dest = MAthPatternRecognitionActivity.class;
                break;

            case AppConstants.ACTIVITY_LOGIC_RIDDLES:
                actvity_name.setText("RIDDLES");
                activity_dest = LogicRiddlesActivity.class;
                break;

            case AppConstants.ACTIVITY_LOGIC_SOLUTIONS:
                actvity_name.setText("SOLUTIONS");
                activity_dest = LogicSolutionsActivity.class;
                break;

            case AppConstants.ACTIVITY_LOGIC_WHO_I_AM:
                actvity_name.setText("WHO I AM");
                activity_dest = LogicWhoIAmActivity.class;
                break;

            case AppConstants.ACTIVITY_MEMORY_WORD_QUIZ:
                actvity_name.setText("WORD QUIZ");
                activity_dest = MemoryWordQuizActivity.class;
                break;

            case AppConstants.ACTIVITY_MEMORY_BUILD_O_WORDS:
                actvity_name.setText("BUILD'O WORDS");
                activity_dest = MemoryBoxOWordsActivity.class;
                break;

            case AppConstants.ACTIVITY_MEMORY_SYNTAX:
            actvity_name.setText("SYNTAX");
                activity_dest = MemorySyntaxActivity.class;
                break;

            default:
                break;
        }
    }


    public void sendIntent(int level){
        intent = new Intent(this, activity_dest);
        intent.putExtra(AppConstants.LEVEL,  String.valueOf(level)); //Optional parameters
        intent.putExtra(AppConstants.GAME_TYPE,  "ACTIVITY"); //Optional parameters
        this.startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.lvl1:
                btnClickSound();
                sendIntent(1);
                break;

            case R.id.lvl2:
                btnClickSound();
                sendIntent(2);
                break;

            case R.id.lvl3:
                btnClickSound();
                sendIntent(3);
                break;

            case R.id.lvl4:
                btnClickSound();
                sendIntent(4);
                break;

            case R.id.lvl5:
                btnClickSound();
                sendIntent(5);
                break;

            case R.id.lvl6:
                btnClickSound();
                sendIntent(6);
                break;

            case R.id.lvl7:
                btnClickSound();
                sendIntent(7);
                break;

            case R.id.lvl8:
                btnClickSound();
                sendIntent(8);
                break;

            case R.id.lvl9:
                btnClickSound();
                sendIntent(9);
                break;

            case R.id.lvl10:
                btnClickSound();
                sendIntent(10);
                break;

            case R.id.lvl11:
                btnClickSound();
                sendIntent(11);
                break;

            case R.id.lvl12:
                btnClickSound();
                sendIntent(12);
                break;

            case R.id.lvl13:
                btnClickSound();
                sendIntent(13);
                break;

            case R.id.lvl14:
                btnClickSound();
                sendIntent(14);
                break;

            case R.id.lvl15:
                btnClickSound();
                sendIntent(15);
                break;

            case R.id.lvl16:
                btnClickSound();
                sendIntent(16);
                break;

            case R.id.lvl17:
                btnClickSound();
                sendIntent(17);
                break;

            case R.id.lvl18:
                btnClickSound();
                sendIntent(18);
                break;

            case R.id.lvl19:
                btnClickSound();
                sendIntent(19);
                break;

            case R.id.lvl20:
                btnClickSound();
                sendIntent(20);
                break;

            case R.id.lvl21:
                btnClickSound();
                sendIntent(21);
                break;

            case R.id.lvl22:
                btnClickSound();
                sendIntent(22);
                break;

            case R.id.lvl23:
                btnClickSound();
                sendIntent(23);
                break;

            case R.id.lvl24:
                btnClickSound();
                sendIntent(24);
                break;

            case R.id.lvl25:
                btnClickSound();
                sendIntent(25);
                break;

            case R.id.lvl26:
                btnClickSound();
                sendIntent(26);
                break;

            case R.id.lvl27:
                btnClickSound();
                sendIntent(27);
                break;

            case R.id.lvl28:
                btnClickSound();
                sendIntent(28);
                break;

            case R.id.lvl29:
                btnClickSound();
                sendIntent(29);
                break;

            case R.id.lvl30:
                btnClickSound();
                sendIntent(30);
                break;

            default:
                break;
        }
    }

    public void mpStart(){
        mp.start();
    }

    public void mpStop(){
        mp.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initUI();
        mediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mpStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mpStop();
    }

    public void mediaPlayer(){
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            mpStart();
        }
        else{
            mpStop();
        }
    }

    public void btnClickSound(){
        MediaPlayer click;
        click = MediaPlayer.create(this, R.raw.click);
        if(Prefs.getBoolean(AppConstants.IS_MUSIC_ON, AppConstants.TRUE)){
            click.start();
        }
//        mp.setLooping(true);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        finish();
    }
}

