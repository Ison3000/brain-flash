package ison.projects.com.brainflash.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ison.projects.com.brainflash.Activities.TriviaActivity;
import ison.projects.com.brainflash.Constants.AppConstants;
import ison.projects.com.brainflash.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TbotFragment extends Fragment implements View.OnClickListener{

    private View mView;
    List<String> list;
    Random randomizer;
    String random;
    TextView trivia;
    Button btn1, btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13,btn14,btn15,
            btn16,btn17,btn18,btn19,btn20,btn21,btn22,btn23,btn24,btn25,btn26,btn27,btn28,btn29,btn30;

    Intent intent;

    public TbotFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_tbot, container, false);
        initUI();
        return mView;
    }

    public void initUI(){
//        trivia = (TextView) mView.findViewById(R.id.trivia);
//        generateArrayList();
        btn1 = (Button) mView.findViewById(R.id.btn1);
        btn1.setOnClickListener(this);
        btn2 = (Button) mView.findViewById(R.id.btn2);
        btn2.setOnClickListener(this);
        btn3 = (Button) mView.findViewById(R.id.btn3);
        btn3.setOnClickListener(this);
        btn4 = (Button) mView.findViewById(R.id.btn4);
        btn4.setOnClickListener(this);
        btn5 = (Button) mView.findViewById(R.id.btn5);
        btn5.setOnClickListener(this);
        btn6 = (Button) mView.findViewById(R.id.btn6);
        btn6.setOnClickListener(this);
        btn7 = (Button) mView.findViewById(R.id.btn7);
        btn7.setOnClickListener(this);
        btn8 = (Button) mView.findViewById(R.id.btn8);
        btn8.setOnClickListener(this);
        btn9 = (Button) mView.findViewById(R.id.btn9);
        btn9.setOnClickListener(this);
        btn10 = (Button) mView.findViewById(R.id.btn10);
        btn10.setOnClickListener(this);
        btn11 = (Button) mView.findViewById(R.id.btn11);
        btn11.setOnClickListener(this);
        btn12 = (Button) mView.findViewById(R.id.btn12);
        btn12.setOnClickListener(this);
        btn13 = (Button) mView.findViewById(R.id.btn13);
        btn13.setOnClickListener(this);
        btn14 = (Button) mView.findViewById(R.id.btn14);
        btn14.setOnClickListener(this);
        btn15 = (Button) mView.findViewById(R.id.btn15);
        btn15.setOnClickListener(this);
        btn16 = (Button) mView.findViewById(R.id.btn16);
        btn16.setOnClickListener(this);
        btn17 = (Button) mView.findViewById(R.id.btn17);
        btn17.setOnClickListener(this);
        btn18 = (Button) mView.findViewById(R.id.btn18);
        btn18.setOnClickListener(this);
        btn19 = (Button) mView.findViewById(R.id.btn19);
        btn19.setOnClickListener(this);
        btn20 = (Button) mView.findViewById(R.id.btn20);
        btn20.setOnClickListener(this);
        btn21 = (Button) mView.findViewById(R.id.btn21);
        btn21.setOnClickListener(this);
        btn22 = (Button) mView.findViewById(R.id.btn22);
        btn22.setOnClickListener(this);
        btn23 = (Button) mView.findViewById(R.id.btn23);
        btn23.setOnClickListener(this);
        btn24 = (Button) mView.findViewById(R.id.btn24);
        btn24.setOnClickListener(this);
        btn25 = (Button) mView.findViewById(R.id.btn25);
        btn25.setOnClickListener(this);
        btn26 = (Button) mView.findViewById(R.id.btn26);
        btn26.setOnClickListener(this);
        btn27 = (Button) mView.findViewById(R.id.btn27);
        btn27.setOnClickListener(this);
        btn28 = (Button) mView.findViewById(R.id.btn28);
        btn28.setOnClickListener(this);
        btn29 = (Button) mView.findViewById(R.id.btn29);
        btn29.setOnClickListener(this);
        btn30 = (Button) mView.findViewById(R.id.btn30);
        btn30.setOnClickListener(this);
    }

        public void sendIntent(int selectedDay){
            intent = new Intent(getActivity(), TriviaActivity.class );
            intent.putExtra(AppConstants.SELECTED_DAY, selectedDay);
            this.startActivity(intent);
        }


    public void generateArrayList(){
        list = new ArrayList<String>();
        list.add(AppConstants.TRIVIA_1);
        list.add(AppConstants.TRIVIA_2);
        list.add(AppConstants.TRIVIA_3);
        list.add(AppConstants.TRIVIA_4);
        list.add(AppConstants.TRIVIA_5);
        list.add(AppConstants.TRIVIA_6);
        list.add(AppConstants.TRIVIA_7);
        list.add(AppConstants.TRIVIA_8);
        list.add(AppConstants.TRIVIA_9);
        list.add(AppConstants.TRIVIA_10);
        list.add(AppConstants.TRIVIA_11);
        list.add(AppConstants.TRIVIA_12);
        list.add(AppConstants.TRIVIA_13);
        list.add(AppConstants.TRIVIA_14);
        list.add(AppConstants.TRIVIA_15);
        list.add(AppConstants.TRIVIA_16);
        list.add(AppConstants.TRIVIA_17);
        list.add(AppConstants.TRIVIA_18);
        list.add(AppConstants.TRIVIA_19);
        list.add(AppConstants.TRIVIA_20);
        list.add(AppConstants.TRIVIA_21);
        list.add(AppConstants.TRIVIA_22);
        list.add(AppConstants.TRIVIA_23);
        list.add(AppConstants.TRIVIA_24);
        list.add(AppConstants.TRIVIA_25);
        list.add(AppConstants.TRIVIA_26);
        list.add(AppConstants.TRIVIA_27);
        list.add(AppConstants.TRIVIA_28);
        list.add(AppConstants.TRIVIA_29);

        randomizer = new Random();
        random = list.get(randomizer.nextInt(list.size()));
        if(Prefs.getInt(AppConstants.TRAINING_ANSWER_COUNT, 0) >= 20) {
            trivia.setText(random);
        }
    }

    @Override
    public void onClick(View view) {
    switch (view.getId()){

        case R.id.btn1:
            sendIntent(1);
            break;
        case R.id.btn2:
            sendIntent(2);
            break;
        case R.id.btn3:
            sendIntent(3);
            break;
        case R.id.btn4:
            sendIntent(4);
            break;
        case R.id.btn5:
            sendIntent(5);
            break;
        case R.id.btn6:
            sendIntent(6);
            break;
        case R.id.btn7:
            sendIntent(7);
            break;
        case R.id.btn8:
            sendIntent(8);
            break;
        case R.id.btn9:
            sendIntent(9);
            break;
        case R.id.btn10:
            sendIntent(10);
            break;
        case R.id.btn11:
            sendIntent(11);
            break;
        case R.id.btn12:
            sendIntent(12);
            break;
        case R.id.btn13:
            sendIntent(13);
            break;
        case R.id.btn14:
            sendIntent(14);
            break;
        case R.id.btn15:
            sendIntent(15);
            break;
        case R.id.btn16:
            sendIntent(16);
            break;
        case R.id.btn17:
            sendIntent(17);
            break;
        case R.id.btn18:
            sendIntent(18);
            break;
        case R.id.btn19:
            sendIntent(19);
            break;
        case R.id.btn20:
            sendIntent(20);
            break;
        case R.id.btn21:
            sendIntent(21);
            break;
        case R.id.btn22:
            sendIntent(22);
            break;
        case R.id.btn23:
            sendIntent(23);
            break;
        case R.id.btn24:
            sendIntent(24);
            break;
        case R.id.btn25:
            sendIntent(25);
            break;
        case R.id.btn26:
            sendIntent(26);
            break;
        case R.id.btn27:
            sendIntent(27);
            break;
        case R.id.btn28:
            sendIntent(28);
            break;
        case R.id.btn29:
            sendIntent(29);
            break;
        case R.id.btn30:
            sendIntent(30);
            break;

        default:
            break;
    }
    }
}
